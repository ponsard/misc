
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <stdint.h>
#include <omp.h>

void cpuapplyGainPedestalandCount(float *data, float *outputImage, 
											float* gain, unsigned int *gCount, 
											size_t width, size_t height, const int NN) 
{



int k=-1;
 

  
#pragma omp parrallel 	
#pragma omp master
{
    k = omp_get_num_threads();
    printf ("Number of Threads requested = %i\n",k);
}
k=0;
#pragma omp parallel
#pragma omp atomic 
    k++;
printf ("Number of Threads counted = %i\n",k);
// for(int N=0; N<NN; N++)
// {
// 	//#pragma omp for
	for (size_t i=0;i<width;i++)
	{
    // #define H 4096
		 
        //#pragma omp simd aligned(data, gain, outputImage,gCount)
        #pragma omp parrallel for
		for(size_t j=0; j<height/4; j++)
		{
            //printf("i %d j %d %f\n",i,j,data[i+j*width]);         

			// float value		= 0.f;
			// uint16_t p		= 0.f;;
			// float raw 	= data[i+j*Width + 2*N*Width*Height]; 
			//uint16_t gn  	= raw >> 14;
			
			// if (gn==1)
			// {
			// 	p = data[i+j*Width +(2*N+1)*Width*Height];
			// }
			//value = ((float) ((raw & 0b0011111111111111) - p) ) * gain[i+j*Width + gn*Width*Height];
			// value = ((float) ((raw ))) * 2.1234; //gain[i+j*Width +Width*Height];
			//outputImage[i+j*Width + N*Width*Height] = value ;
			outputImage[i+j*width + NN*width*height] = data[i+j*width + NN*width*height] ;

			// if (value>1.f && value<6.0f) 
			// 	gCount[N]++;
		}
	}
	
// }

}

void main()
{
    size_t width            = 1024,
        height              = 512,
        NN                  = 16;
    int page_size	        = sysconf(_SC_PAGESIZE);

    float* data             = (float*)          memalign(page_size, width*height*NN*4);
    float* outputImage      = (float*)          memalign(page_size, width*height*NN*4);
    float* gain             = (float* )         memalign(page_size, width*height*4*4); 
    unsigned int* gCount    = (unsigned int*)   memalign(page_size, NN*4);

    printf("data %p output %p\n",data,outputImage);         

    for (int i=0;i<width*height*NN;i++)
        data[i]=(float)i;
    for (int i=0;i<width*height;i++)
        gain[i]=(float)i;

//    for (size_t i=0;i<width;i++)
// 		for(size_t j=0;j<height;j++)
//             printf("i %d j %d %f\n",i,j,data[i+j*width]); 
   
    printf("go\n");         
    for (int i=0;i<NN;i++)
        cpuapplyGainPedestalandCount(data, outputImage, 
                                    gain, gCount, 
                                    width, height, i) ;

    for (int i=0;i<width*height*NN;i++)
        if (data[i] != outputImage[i])
        {
            printf("mismatch %d\n",i);
            break;
        }

}