	.file	"simd.c"
	.text
	.globl	cpuapplyGainPedestalandCount
	.type	cpuapplyGainPedestalandCount, @function
cpuapplyGainPedestalandCount:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movl	%r8d, -52(%rbp)
	movl	%r9d, -56(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L2
.L3:
	movl	-4(%rbp), %eax
	cltq
	leaq	0(,%rax,4), %rdx
	movq	-32(%rbp), %rax
	addq	%rdx, %rax
	movl	-4(%rbp), %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	leaq	0(,%rdx,4), %rcx
	movq	-24(%rbp), %rdx
	addq	%rcx, %rdx
	movss	(%rdx), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	.LC0(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rax)
	addl	$1, -4(%rbp)
.L2:
	cmpl	$4095, -4(%rbp)
	jle	.L3
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	cpuapplyGainPedestalandCount, .-cpuapplyGainPedestalandCount
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	$1024, -4(%rbp)
	movl	$512, -8(%rbp)
	movl	$16, -12(%rbp)
	movl	$30, %edi
	call	sysconf@PLT
	movl	%eax, -16(%rbp)
	movl	-4(%rbp), %eax
	imull	-8(%rbp), %eax
	imull	-12(%rbp), %eax
	addl	%eax, %eax
	movslq	%eax, %rdx
	movl	-16(%rbp), %eax
	cltq
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	memalign@PLT
	movq	%rax, -24(%rbp)
	movl	-4(%rbp), %eax
	imull	-8(%rbp), %eax
	imull	-12(%rbp), %eax
	sall	$2, %eax
	movslq	%eax, %rdx
	movl	-16(%rbp), %eax
	cltq
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	memalign@PLT
	movq	%rax, -32(%rbp)
	movl	-4(%rbp), %eax
	imull	-8(%rbp), %eax
	sall	$2, %eax
	movslq	%eax, %rdx
	movl	-16(%rbp), %eax
	cltq
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	memalign@PLT
	movq	%rax, -40(%rbp)
	movl	-12(%rbp), %eax
	sall	$2, %eax
	movslq	%eax, %rdx
	movl	-16(%rbp), %eax
	cltq
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	memalign@PLT
	movq	%rax, -48(%rbp)
	movl	-8(%rbp), %r9d
	movl	-4(%rbp), %r8d
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	-32(%rbp), %rsi
	movq	-24(%rbp), %rax
	subq	$8, %rsp
	movl	-12(%rbp), %edi
	pushq	%rdi
	movq	%rax, %rdi
	call	cpuapplyGainPedestalandCount
	addq	$16, %rsp
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC0:
	.long	3367254360
	.long	1072938614
	.ident	"GCC: (Debian 6.3.0-18+deb9u1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",@progbits
