/*
 * 
 * 
 
nvcc -ccbin g++-6 -m64  \
-I /media/ponsard/NVMESSD/NVIDIA_CUDA-8.0_Samples/common/inc \
-lcuda -libverbs \
-gencode arch=compute_50,code=sm_50 \
-gencode arch=compute_52,code=sm_52 \
-gencode arch=compute_60,code=sm_60 \
-gencode arch=compute_60,code=compute_60 \
-o cudabw ./cudabw.cu


*
* 
* 
*/ 
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <unistd.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", 
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}


__global__ void rightRotation(char *inputImage,char *outputImage,int Width,int Height)
{   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x,ii;
    unsigned int j = blockIdx.y*blockDim.y + threadIdx.y,jj;
    // (Height-1 - j,Width-1 - i)    <=  (i,j)
    ii = j; //Height-1 - j;
    jj = Width-1 - i;

    if (i < Width && j < Height && ii < Height && jj < Width) {
        outputImage[3*(ii+jj* Height)+0] = inputImage[3*(i+j* Width)+0];
        outputImage[3*(ii+jj* Height)+1] = inputImage[3*(i+j* Width)+1];
        outputImage[3*(ii+jj* Height)+2] = inputImage[3*(i+j* Width)+2];
        }
}




cudaStream_t 			s0,s1,s2,s3;

int width=512,height=512;
float *stats;

void profileCopies(char        *h_a, 
                   char        *h_b, 
                   char        *d, 
                   char        *d2, 
                   unsigned int  bytes,
                   int nImg, int iter,int s
                   )
{
	dim3 dimBlock(512,1, 1);
	dim3 dimGrid (1,512, 1);

	// events for timing
	cudaEvent_t startEvent, stopEvent; 

	checkCuda( cudaEventCreate(&startEvent) );
	checkCuda( cudaEventCreate(&stopEvent) );
	checkCuda( cudaEventRecord(startEvent, 0) );

	for (int k=0;k<iter;k++) 
	{
		checkCuda( cudaMemcpyAsync(	d, 
									h_a + k*bytes*nImg,
									bytes*nImg, 
									cudaMemcpyHostToDevice) );
		for(int i=0;i<nImg;i++)
			rightRotation <<< dimGrid, dimBlock >>>( d+i*bytes, d2+i*bytes, width,height );
		
		checkCuda( cudaMemcpyAsync(	h_b + k*bytes*nImg, 
									d2,
									bytes*nImg, 
									cudaMemcpyDeviceToHost) );
	}
	checkCuda( cudaEventRecord(stopEvent, 0) );
	checkCuda( cudaEventSynchronize(stopEvent) );

	float time;
	checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
	stats[s] =  	8*(float)bytes*iter*nImg * 1e-6 / time;
	printf("H->D K D->H bandwidth (Gb/s): %f\n", stats[s]);
	checkCuda( cudaEventDestroy(startEvent) );
	checkCuda( cudaEventDestroy(stopEvent) );
}

int main()
{

	int devId = 1;
	checkCuda(cudaSetDevice(devId));

	//~ checkCuda(cudaStreamCreate(&s0));
	//~ checkCuda(cudaStreamCreate(&s1));
	//~ checkCuda(cudaStreamCreate(&s2));
	//~ checkCuda(cudaStreamCreate(&s3));

#define STP 10
	stats=(float*)malloc(STP*sizeof(float));
	
	for(int stp=1; stp<STP;stp++) 
	{
		int bytes = 512*4*512;
		int N = 10*stp;
		int iter = 100/stp;
		// host arrays
		char *h_aPinned, *h_bPinned;

		// device array
		char *d_a, *d_b;
		// allocate and initialize
		checkCuda( cudaMallocHost((void**)&h_aPinned, bytes*N*iter) ); // host pinned
		checkCuda( cudaMallocHost((void**)&h_bPinned, bytes*N*iter) ); // host pinned
		checkCuda( cudaMalloc((void**)&d_a, bytes*N) );           // device
		checkCuda( cudaMalloc((void**)&d_b, bytes*N) );           // device

		//~ memset(h_bPinned, 0, bytes*N);
		memset(stats,0,STP*sizeof(float));

		// output device info and transfer size
		cudaDeviceProp prop;
		checkCuda( cudaGetDeviceProperties(&prop, devId) );

		printf("\nDevice: %s  step %d iter %d\n", prop.name, stp, iter);

		int f0 = open("/tmp/lena100.data", O_RDWR|O_CREAT,S_IRWXU);
		read(f0,h_aPinned,bytes*N);
		close(f0);
		 
		profileCopies(h_aPinned, h_bPinned, d_a, d_b,bytes,N,iter,stp);
		
		int f2 = open("/tmp/stat.data", O_RDWR|O_CREAT,S_IRWXU);
		write(f2, stats, STP*sizeof(float));	
		close(f2);

		int f1 = open("/tmp/res.data", O_RDWR|O_CREAT,S_IRWXU);
		unsigned long int res = 0,totalSize = N*iter*bytes;
		do
		{
			res += write(f1, h_bPinned+res, totalSize-res);	
			printf("saving... %d\n",res);
		}
		while (res < totalSize);
		
		close(f1);
		
		cudaFree(d_a);
		cudaFree(d_b);
		cudaFreeHost(h_aPinned);
		cudaFreeHost(h_bPinned);
	}
	return 0;
}
