#ifndef RDMA_COMMON_H
#define RDMA_COMMON_H


#define RDMA_BUFFER_SIZE 	512
#define N_MSG				32

#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <rdma/rdma_cma.h>

#define TEST_NZ(x) do { if ( (x)) die("error: " #x " failed (returned non-zero)." ); } while (0)
#define TEST_Z(x)  do { if (!(x)) die("error: " #x " failed (returned zero/null)."); } while (0)

enum mode {
  M_WRITE,
  M_READ
};


struct message {
  enum {
    MSG_MR,
    MSG_DONE
  } type;

  union {
    struct ibv_mr mr;
  } data;
};

struct context {
  struct ibv_context *ctx;
  struct ibv_pd *pd;
  struct ibv_cq *cq;
  struct ibv_comp_channel *comp_channel;

  pthread_t cq_poller_thread;
};

struct connection {
  struct rdma_cm_id *id;
  struct ibv_qp *qp;

  int connected;

  struct ibv_mr *recv_mr;
  struct ibv_mr *rdma_local_mr;

  struct ibv_mr peer_mr;

  struct message *recv_msg;

  char *rdma_local_region;

 
  enum {
    RS_INIT,
    RS_MR_RECV,
    RS_DONE_RECV,
    RS_END
  } recv_state;
  
};

void die(const char *reason);
void build_connection(struct rdma_cm_id *id);
void build_params(struct rdma_conn_param *params);
void destroy_connection(void *context);
void on_connect(void *context);

#endif
