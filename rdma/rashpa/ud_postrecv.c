#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>

#include <infiniband/verbs.h>

int main()
{
struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
	
struct ibv_comp_channel *channel;

struct ibv_pd *pd;
struct ibv_mr *mr,*mr2,*mr3,*mr4;
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size=sysconf(_SC_PAGESIZE);
int size       = 512; //less than MTU
int send_flags = IBV_SEND_SIGNALED;
int rx_depth   = 500;
//FIXME
int ib_port = 1 ;


//memory allocation
char *buf = memalign(page_size, size + 40);
char *buf2 = memalign(page_size, size + 40);
char *buf3 = memalign(page_size, size + 40);
char *buf4 = memalign(page_size, size + 40);
if (!buf) {
	fprintf(stderr, "Couldn't allocate work buf.\n");
	goto _end;
}
memset(buf,  '-', size + 40);
memset(buf2, '_', size + 40);

//get device
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return -1;
	}

ib_dev = dev_list[0];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) {
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	goto clean_buffer;
}
printf("using device %s\n",ibv_get_device_name(ib_dev));

channel = ibv_create_comp_channel(context);
if (!channel) {
	fprintf(stderr, "Error, ibv_create_comp_channel() failed\n");
	return -1;
}
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) {
	fprintf(stderr, "Couldn't allocate PD\n");
	goto clean_comp_channel;
}

//Memory Region
mr =  ibv_reg_mr(pd, buf,  size + 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr2 = ibv_reg_mr(pd, buf2, size + 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr3 = ibv_reg_mr(pd, buf3, size + 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr4 = ibv_reg_mr(pd, buf4, size + 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
if (!mr) {
	fprintf(stderr, "Couldn't register MR\n");
	goto clean_pd;
}
//Completion Queue
printf("ibv_create_cq %p %p %d\n",context,channel,rx_depth);

cq = ibv_create_cq(context, 100, NULL,channel, 0);
	if (!cq) {
		fprintf(stderr, "Couldn't create CQ\n");
		goto clean_mr;
	}
printf("ibv_create_cq done \n");
	

//Queue Pair

printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 10;
qp_init_attr.cap.max_send_sge = 10;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 10;
qp_init_attr.cap.max_recv_sge = 10;
qp_init_attr.qp_type = IBV_QPT_UD; 
printf("ibv_create_qp\n");
qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}
printf("ibv_create_qp %d\n",qp->qp_num);

struct ibv_qp_attr attr = {
			.qp_state        = IBV_QPS_INIT,
			.pkey_index      = 0,
			.port_num        = ib_port, //this is ib_port
			.qkey            = 0xCAFECAFE //remote_qpn the other side
			//.qp_access_flags = 0 //IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE 

		};
printf("ibv_modify_qp to INIT\n");
 
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY //IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}

//Modify QP to RTR
printf("ibv_modify_qp to RTR\n");

memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) {
		fprintf(stderr, "Failed to modify QP to RTR\n");
		return -1;
	}

//Post Recv Work Request
struct ibv_sge list [4];
list[0].addr	= (uintptr_t) buf;
list[0].length  = size+40;
list[0].lkey	= mr->lkey;
list[1].addr	= (uintptr_t) buf2;
list[1].length  = size;
list[1].lkey	= mr2->lkey;

list[2].addr	= (uintptr_t) buf3;
list[2].length  = size+40;
list[2].lkey	= mr3->lkey;
list[3].addr	= (uintptr_t) buf4;
list[3].length  = size;
list[3].lkey	= mr4->lkey;

struct ibv_recv_wr wr = {
		.wr_id	    = 0x1234,
		.sg_list    = &list[0],
		.num_sge    = 2,
	};
struct ibv_recv_wr wr2 = {
		.wr_id	    = 0x1235,
		.sg_list    = &list[2],
		.num_sge    = 2,
	};
	


struct ibv_recv_wr *bad_wr;
printf("ibv_post_recv\n");

if (ibv_post_recv(qp, &wr, &bad_wr)) return -1;
 if (ibv_post_recv(qp, &wr2, &bad_wr)) return -1;
			
			
int i ;

puts("getchar");			
getchar();
for( i=0;i<50;i++) 
	printf("%x ",*(buf+i) & 0xff);
puts("\nbuf0");
puts(buf +  40);			
puts("buf2");
puts(buf2 + 0);	
puts("buf3");
puts(buf3 +  40);			
puts("buf4");
puts(buf4 + 0);			
		 

clean_qp:
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	ibv_destroy_cq(cq);

clean_mr:
	ibv_dereg_mr(mr);

clean_pd:
	ibv_dealloc_pd(pd);

clean_comp_channel:
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	ibv_close_device(context);

clean_buffer:
	free(buf);

_end:
	return -1;

}
