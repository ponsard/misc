/*
 * 
 * 
 * 
 * 
 * 
 * 
 * gcc read-write-client.c rdma-common.c -o read-write-client -libverbs -lrdmacm -lpthread
 * 
 * 
 */
 
#include "rdma-common.h"

const int TIMEOUT_IN_MS = 500; /* ms */

static int on_addr_resolved(struct rdma_cm_id *id);
static int on_connection(struct rdma_cm_id *id);
static int on_disconnect(struct rdma_cm_id *id);
static int on_event(struct rdma_cm_event *event);
static int on_route_resolved(struct rdma_cm_id *id);

int main(int argc, char **argv)
{
  struct addrinfo *addr;
  struct rdma_cm_event *event = NULL;
  struct rdma_cm_id *conn= NULL;
  struct rdma_event_channel *ec = NULL;

  printf("getaddrinfo\n");
  TEST_NZ(getaddrinfo(argv[1], argv[2], NULL, &addr));
  printf("rdma_create_event_channel\n");

  TEST_Z(ec = rdma_create_event_channel());
  //FIXME test RDMA_PS_UDP
  //TEST_NZ(rdma_create_id(ec, &conn, NULL, RDMA_PS_TCP));
  printf("rdma_create_id\n");
  TEST_NZ(rdma_create_id(ec, &conn, NULL, RDMA_PS_UDP));
  TEST_NZ(rdma_resolve_addr(conn, NULL, addr->ai_addr, TIMEOUT_IN_MS));

  freeaddrinfo(addr);
  printf("rdma_get_cm_event\n");

  while (rdma_get_cm_event(ec, &event) == 0) {
    struct rdma_cm_event event_copy;
  printf("ev loop\n");

    memcpy(&event_copy, event, sizeof(*event));
    rdma_ack_cm_event(event);

    if (on_event(&event_copy))
      break;
  }
  rdma_destroy_event_channel(ec);
  return 0;
}

int on_addr_resolved(struct rdma_cm_id *id)
{
  printf("address resolved.\n");

  build_connection(id);
  for(int i=0; i<N_MSG*4; i++) {
	char * s = ((struct connection *)(id->context))->rdma_local_region;
	sprintf(s + i*RDMA_BUFFER_SIZE/4, 
		"message %d sent from ACTIVE CLIENT pid %d", i,getpid());
	}
  TEST_NZ(rdma_resolve_route(id, TIMEOUT_IN_MS)); 
  return 0;
}

int on_connection(struct rdma_cm_id *id)
{
  printf("on_connection.\n");

  on_connect(id->context);
  return 0;
}

int on_disconnect(struct rdma_cm_id *id)
{
  printf("on_disconnect\n");

  destroy_connection(id->context);
  return 1; /* exit event loop */
}

int on_event(struct rdma_cm_event *event)
{
  int r = 0;

  if (event->event == 1)
    printf("on_??????\n");

  if (event->event == RDMA_CM_EVENT_ADDR_RESOLVED)
    r = on_addr_resolved(event->id);
  else if (event->event == RDMA_CM_EVENT_ROUTE_RESOLVED)
    r = on_route_resolved(event->id);
  else if (event->event == RDMA_CM_EVENT_ESTABLISHED)
    r = on_connection(event->id);
  else if (event->event == RDMA_CM_EVENT_DISCONNECTED)
    r = on_disconnect(event->id);
  else {
    fprintf(stderr, "on_event: %d\n", event->event);
    die("on_event: unknown event.");
  }
  return r;
}

int on_route_resolved(struct rdma_cm_id *id)
{
  struct rdma_conn_param cm_params;

  printf("on_route_resolved\n");
  build_params(&cm_params);
  TEST_NZ(rdma_connect(id, &cm_params));
  return 0;
}
