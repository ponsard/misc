#include "rdma-common.h"

static void build_context(struct ibv_context *verbs);
static void build_qp_attr(struct ibv_qp_init_attr *qp_attr);
static void on_completion(struct ibv_wc *);
static void * poll_cq(void *);
static void post_receives(struct connection *conn);
static void register_memory(struct connection *conn);

static struct context *s_ctx = NULL;

void die(const char *reason)
{
  fprintf(stderr, "%s\n", reason);
  exit(EXIT_FAILURE);
}

void build_connection(struct rdma_cm_id *id)
{
  struct connection *conn;
  struct ibv_qp_init_attr qp_attr;

  build_context(id->verbs);
  build_qp_attr(&qp_attr);

  TEST_NZ(rdma_create_qp(id, s_ctx->pd, &qp_attr));

  id->context = conn = (struct connection *)malloc(sizeof(struct connection));

  conn->id = id;
  conn->qp = id->qp;
  
  conn->recv_state = RS_INIT;

  conn->connected = 0;

  register_memory(conn);
  post_receives(conn);
}

void build_context(struct ibv_context *verbs)
{
printf("build_context\n");

  if (s_ctx) {
    if (s_ctx->ctx != verbs)
      die("cannot handle events in more than one context.");

    return;
  }

  s_ctx = (struct context *)malloc(sizeof(struct context));

  s_ctx->ctx = verbs;

  TEST_Z(s_ctx->pd = ibv_alloc_pd(s_ctx->ctx));
  TEST_Z(s_ctx->comp_channel = ibv_create_comp_channel(s_ctx->ctx));
  TEST_Z(s_ctx->cq = ibv_create_cq(s_ctx->ctx, 10, NULL, s_ctx->comp_channel, 0)); /* cqe=10 is arbitrary */
  TEST_NZ(ibv_req_notify_cq(s_ctx->cq, 0));

  TEST_NZ(pthread_create(&s_ctx->cq_poller_thread, NULL, poll_cq, NULL));
}

void build_params(struct rdma_conn_param *params)
{
printf("build_params\n");

  memset(params, 0, sizeof(*params));

  params->initiator_depth = params->responder_resources = 1;
  params->rnr_retry_count = 7; /* infinite retry */
}

void build_qp_attr(struct ibv_qp_init_attr *qp_attr)
{
printf("build_qp_attr\n");

  memset(qp_attr, 0, sizeof(*qp_attr));

  qp_attr->send_cq = s_ctx->cq;
  qp_attr->recv_cq = s_ctx->cq;
  //FIXME
  qp_attr->qp_type = IBV_QPT_RC;
  //qp_attr->qp_type = IBV_QPT_UD;

  qp_attr->cap.max_send_wr = 10;
  qp_attr->cap.max_recv_wr = 10;
  qp_attr->cap.max_send_sge = N_MSG;
  qp_attr->cap.max_recv_sge = 1;
}

void destroy_connection(void *context)
{
printf("destroy_connection\n");

  struct connection *conn = (struct connection *)context;

  rdma_destroy_qp(conn->id);

  ibv_dereg_mr(conn->recv_mr);
  ibv_dereg_mr(conn->rdma_local_mr);

  free(conn->recv_msg);
  free(conn->rdma_local_region);

  rdma_destroy_id(conn->id);

  free(conn);
}

void on_completion(struct ibv_wc *wc)
{
printf("on_completion\n");

  struct connection *conn = (struct connection *)(uintptr_t)wc->wr_id;

  if (wc->status != IBV_WC_SUCCESS)
    die("on_completion: status is not IBV_WC_SUCCESS.");

  printf("IBV_WC_SUCCESS\n");
  conn->recv_state ++;
  
  if (conn->recv_state == RS_END) {printf("RS_END\n");getchar();}
  
  if (conn->recv_state == RS_MR_RECV) {
    //FIXME
    printf("RS_MR_RECV\n");

    if (conn->recv_msg->type == MSG_MR) {
      memcpy(&conn->peer_mr, &conn->recv_msg->data.mr, sizeof(conn->peer_mr));
      conn->recv_state = RS_DONE_RECV;
    }
  }

  if ( conn->recv_state == RS_DONE_RECV) {
    struct ibv_send_wr wr,wr2,wr3,wr4, *bad_wr = NULL;
    struct ibv_sge sge[N_MSG],sge2[N_MSG],sge3[N_MSG],sge4[N_MSG];
    
    printf("RS_DONE_RECV writing message to remote memory...\n");
    memset(&wr, 0, sizeof(wr));

    wr.wr_id = (uintptr_t)conn;
    wr.next= &wr3;
    wr.opcode = IBV_WR_RDMA_WRITE;
    wr.sg_list = &sge[0];
    wr.num_sge = N_MSG;
    wr.send_flags = IBV_SEND_SIGNALED;
    wr.wr.rdma.remote_addr = (uintptr_t)conn->peer_mr.addr+N_MSG*RDMA_BUFFER_SIZE*0/4;
    wr.wr.rdma.rkey = conn->peer_mr.rkey;

    wr2.wr_id = 0xcaffe0; //(uintptr_t)conn;
    wr2.next= &wr4;
    wr2.opcode = IBV_WR_RDMA_WRITE;
    wr2.sg_list = &sge2[0];
    wr2.num_sge = N_MSG;
    wr2.send_flags = IBV_SEND_SIGNALED;
    wr2.wr.rdma.remote_addr = (uintptr_t)conn->peer_mr.addr+N_MSG*RDMA_BUFFER_SIZE*1/4;
    wr2.wr.rdma.rkey = conn->peer_mr.rkey;

    wr3.wr_id = 0xcaffe1; //(uintptr_t)conn;
    wr3.next= &wr2;
    wr3.opcode = IBV_WR_RDMA_WRITE;
    wr3.sg_list = &sge3[0];
    wr3.num_sge = N_MSG;
    wr3.send_flags = IBV_SEND_SIGNALED;
    wr3.wr.rdma.remote_addr = (uintptr_t)conn->peer_mr.addr+N_MSG*RDMA_BUFFER_SIZE*2/4;
    wr3.wr.rdma.rkey = conn->peer_mr.rkey;


    wr4.wr_id = 0xcaffe2; //(uintptr_t)conn;
    wr4.next= NULL;
    wr4.opcode = IBV_WR_RDMA_WRITE;
    wr4.sg_list = &sge4[0];
    wr4.num_sge = N_MSG;
    wr4.send_flags = IBV_SEND_SIGNALED;
    wr4.wr.rdma.remote_addr = (uintptr_t)conn->peer_mr.addr+N_MSG*RDMA_BUFFER_SIZE*3/4;
    wr4.wr.rdma.rkey = conn->peer_mr.rkey;


    for(int i=0;i<N_MSG;i++) {
		sge[i].addr = (uintptr_t)(conn->rdma_local_region + i*RDMA_BUFFER_SIZE/4+N_MSG*RDMA_BUFFER_SIZE*0/4);
		sge[i].length = RDMA_BUFFER_SIZE/4;
		sge[i].lkey = conn->rdma_local_mr->lkey;
    }
    for(int i=0;i<N_MSG;i++) {
		sge2[i].addr = (uintptr_t)(conn->rdma_local_region + i*RDMA_BUFFER_SIZE/4+N_MSG*RDMA_BUFFER_SIZE*1/4);
		sge2[i].length = RDMA_BUFFER_SIZE/4;
		sge2[i].lkey = conn->rdma_local_mr->lkey;
    }

    for(int i=N_MSG-1;i>=0;i--) {
		sge3[i].addr = (uintptr_t)(conn->rdma_local_region + i*RDMA_BUFFER_SIZE/4+N_MSG*RDMA_BUFFER_SIZE*2/4);
		sge3[i].length = RDMA_BUFFER_SIZE/4;
		sge3[i].lkey = conn->rdma_local_mr->lkey;
    }
    for(int i=0;i<N_MSG;i++) {
		sge4[i].addr = (uintptr_t)(conn->rdma_local_region + i*RDMA_BUFFER_SIZE/4+N_MSG*RDMA_BUFFER_SIZE*3/4);
		sge4[i].length = RDMA_BUFFER_SIZE/4;
		sge4[i].lkey = conn->rdma_local_mr->lkey;
    }
getchar();
    TEST_NZ(ibv_post_send(conn->qp, &wr, &bad_wr));
  } 
}

void on_connect(void *context)
{
  printf("on_connect\n");
	
  ((struct connection *)context)->connected = 1;
}

void * poll_cq(void *ctx)
{
  struct ibv_cq *cq;
  struct ibv_wc wc;

  while (1) {
    TEST_NZ(ibv_get_cq_event(s_ctx->comp_channel, &cq, &ctx));
    ibv_ack_cq_events(cq, 1);
    TEST_NZ(ibv_req_notify_cq(cq, 0));

    while (ibv_poll_cq(cq, 1, &wc))
      on_completion(&wc);
  }

  return NULL;
}

void post_receives(struct connection *conn)
{
printf("in post_receives\n");

  struct ibv_recv_wr wr, *bad_wr = NULL;
  struct ibv_sge sge;

  wr.wr_id = (uintptr_t)conn;
  wr.next = NULL;
  wr.sg_list = &sge;
  wr.num_sge = 1;

  sge.addr = (uintptr_t)conn->recv_msg;
  sge.length = sizeof(struct message);
  sge.lkey = conn->recv_mr->lkey;

  TEST_NZ(ibv_post_recv(conn->qp, &wr, &bad_wr));
}

void register_memory(struct connection *conn)
{
  conn->recv_msg = malloc(sizeof(struct message));
  conn->rdma_local_region  = malloc(N_MSG*RDMA_BUFFER_SIZE);

  TEST_Z(conn->recv_mr = ibv_reg_mr(
    s_ctx->pd, 
    conn->recv_msg, 
    sizeof(struct message), 
    IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE));

  TEST_Z(conn->rdma_local_mr = ibv_reg_mr(
    s_ctx->pd, 
    conn->rdma_local_region, 
    N_MSG*RDMA_BUFFER_SIZE, 
    IBV_ACCESS_LOCAL_WRITE));
}

