/*
 * 
 * 
 * gcc ud_postrecv.c -o ud_postrecv -libverbs
 * 
 * 
 */

#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <infiniband/verbs.h>

#include "ud.h"
#include "../../../romulu/controller/src/romulu_nvidia.h"

CUdevice 			cuDevice;
CUcontext 			cuContext;
CUdeviceptr 		d_A;

void displayProfilingResult(struct timespec *end, struct timespec *start, int n, int l, int i)
{
	double 	dt_ms = (end->tv_nsec-start->tv_nsec)/1000000.0 + (end->tv_sec-start->tv_sec)*1000.0;
	double 	gbps = (double) i * n * l * 8 / dt_ms * 1e3 / 1e9;
	double 	pps  = (double) i * n  / dt_ms;
	double 	fps  = (double) n / dt_ms * 1000;

	printf("\nBW %.1f Gbits/s, %.1fk pps, %.1f fps using single GPU core. (%d images) dt_ms %f ms\n\n",
	 							gbps, pps, fps, i,dt_ms);
}
void select_cpu()
{
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(1, &set);
	CPU_SET(3, &set);
	CPU_SET(5, &set);
	CPU_SET(7, &set);
	sched_setaffinity(0, sizeof(set), &set);
	unsigned cpu = sched_getcpu();
	printf("thread using cpu %d\n", cpu);
}


void init_nvidia_cuda_gpu_device(int alloc_size) 
{
	
	int gpuId 				= 1;
	printf("init_nvidia_cuda_gpu_device devID %d\n", gpuId);

	CUresult error = cuInit(0);
	if (error != CUDA_SUCCESS) {
		printf("cuInit(0) returned %d\n", error);
		exit(1);
	}

	int deviceCount = 0;
	error = cuDeviceGetCount(&deviceCount);
	if (error != CUDA_SUCCESS) {
		printf("cuDeviceGetCount() returned %d\n", error);
		exit(1);
	}
	
	printf("cudaSetDeviceFlags\n");
	cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);
	cudaGetLastError();
	/* pick up device with zero ordinal (default, or devID) */
	CUCHECK(cuDeviceGet(&cuDevice, gpuId));

	char name[128];
	CUCHECK(cuDeviceGetName(name, sizeof(name), gpuId));
	printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);

	/* Create context */
	cuContext=NULL;
	printf("cuCtxCreate\n");
	error = cuCtxCreate(&cuContext, CU_CTX_MAP_HOST, cuDevice);
	if (error != CUDA_SUCCESS) {
		printf("cuCtxCreate() error=%d\n", error);
		return ;
	}
	printf("cuCtxSetCurrent\n");
	error = cuCtxSetCurrent(cuContext);
	if (error != CUDA_SUCCESS)
	{
		printf("cuCtxSetCurrent() error=%d\n", error);
		return ;
	}

	
	CUCHECK( cuMemAlloc(&d_A, alloc_size) );  
		
	printf("RDMA capable GPU, allocated GPU memory at %p\n", d_A);
}




int main(int argc, char**argv)
{
int gpu		= atoi(argv[1]);
int d		= atoi(argv[2]);
int nWr 	= atoi(argv[3]);
int bufLen 	= atoi(argv[4]);
int nIter 	= atoi(argv[5]);

int memorySize = nWr*bufLen;

select_cpu();

struct timespec startTime, endTime;

struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
struct ibv_comp_channel *channel=NULL;
struct ibv_pd *pd;
struct ibv_mr *mr,*mr_gth; //*mr2,*mr3,*mr4;
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size=sysconf(_SC_PAGESIZE);
int ib_port = 1 ;

//dest memory allocation backed by HUGE PAGES
char devname[128]="/dev/hugepages/rashpadata";

int fd = open(devname, O_RDWR|O_CREAT, 0755);
if (fd < 0) 
{
	perror("/dev/hugepages/rashpadata Open failed");
	exit(1);
}
void* addr ; 
if (gpu) 
{
	init_nvidia_cuda_gpu_device(memorySize);
	addr = (void*) d_A;
}
else
	addr = mmap(	NULL,
					memorySize ,
					PROT_READ|PROT_WRITE,
					MAP_SHARED|MAP_HUGETLB,
					fd,
					0);

if (addr == MAP_FAILED)
{
	perror("mmap ");
	printf("of %d error\n",nWr * bufLen );
	unlink("/dev/hugepages/rashpadata");
	exit(1);
}	


//40 bytes extra bytes memory allocation
char *buf_gth  = memalign(page_size, 40);

//get devices list
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return -1;
	}

ib_dev = dev_list[d];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) 
{
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	goto clean_buffer;
}
printf("using device %s\n",ibv_get_device_name(ib_dev));
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) 
{
	fprintf(stderr, "Couldn't allocate PD\n");
	goto clean_comp_channel;
}

//Memory Region
mr =  ibv_reg_mr(pd, addr,  memorySize, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr_gth = ibv_reg_mr(pd, buf_gth, 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);

if (!mr) 
{
	fprintf(stderr, "Couldn't register MR\n");
	goto clean_pd;
}
//Completion Queue
printf("ibv_create_cq\n");
cq = ibv_create_cq(context, nWr, NULL,NULL, 0);
if (!cq) 
{
	fprintf(stderr, "Couldn't create CQ\n");
	goto clean_mr;
}

//Queue Pair
printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 1;
qp_init_attr.cap.max_send_sge = 1;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = nWr;
qp_init_attr.cap.max_recv_sge = 2;
qp_init_attr.qp_type = IBV_QPT_UD; 

qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}
printf("ibv_create_qp qnum %d\n",qp->qp_num);
struct ibv_qp_attr attr = {
			.qp_state        = IBV_QPS_INIT,
			.pkey_index      = 0,
			.port_num        = ib_port,
			.qkey            = 0xCAFECAFE 
			//.qp_access_flags = 0 //IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE 

		};

printf("ibv_modify_qp to INIT\n");
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY //IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}

//Modify QP to RTR
printf("ibv_modify_qp to RTR\n");
memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) 
{
	fprintf(stderr, "Failed to modify QP to RTR\n");
	return -1;
}

//Post Recv Work Request
struct ibv_sge		*sg = (struct ibv_sge*)		memalign(page_size,2*nWr*sizeof(struct ibv_sge));
struct ibv_recv_wr	*wr	= (struct ibv_recv_wr*)	memalign(page_size,nWr*sizeof(struct ibv_recv_wr)),*bad_wr;
struct ibv_wc 		*wc	= (struct ibv_wc*)		memalign(page_size,nWr*sizeof(struct ibv_wc));
memset(sg,0,2*nWr*sizeof(struct ibv_sge));
memset(wr,0,nWr*sizeof(struct ibv_recv_wr));

for (int i = 0 ; i < nWr ; i++)
{
	wr[i].wr_id     	= i;
	wr[i].next 			= (i==(nWr -1)) ? NULL : &wr[i+1];
	wr[i].sg_list   	= &(sg[2*i]);
	wr[i].num_sge 		= 2;
	sg[2*i].length  	= 40;
	sg[2*i].lkey	  	= mr_gth->lkey;
	sg[2*i].addr 		= (uint64_t) buf_gth;
	sg[2*i+1].length 	= bufLen;
	sg[2*i+1].lkey		= mr->lkey;
	sg[2*i+1].addr 		= (uint64_t) addr + i * bufLen;
}

//post recv wr
int start=1;
int diff=0;
for (int i=0;i<nIter;i++)
{
	// if (!gpu) 
	// for(int i=0;i<bufLen;i++) 
	// {	
	// 	*((char*)addr+(nWr-1)*bufLen+i)=0;
	// }

	if (ibv_post_recv(qp, wr, &bad_wr)) return -1;
	// if (i%10==0) 
	// 	printf("ibv_post_recv %d, ready... \n",i);

	int p=0,n;
	do 
	{
		//printf("ibv_poolv %d\n",p);
		n = ibv_poll_cq(cq, nWr-p, wc);
		if (n>0 && start) 
		{
			clock_gettime(CLOCK_MONOTONIC, &startTime);
			start=0;
		}

		p +=n;
	} while (p < nWr);

	int psn = ntohl(wc[n-1].imm_data)-0x123456;
	// int nCompletion = i*nWr*n
	if (psn+1 - (i+1)*nWr > diff ) 
	{
		printf("diff %d iter %d\n",psn+1 - (i+1)*nWr,i);
		diff = psn+1 - (i+1)*nWr;
		//i=1;
	}
}
clock_gettime(CLOCK_MONOTONIC, &endTime);
displayProfilingResult(&endTime, &startTime, nWr, bufLen, nIter);

//display data	
if (!gpu) 
{
	for(int i=0;i<bufLen;i++) 
	{
		printf("%c",*(((char*)addr)+i) & 0xff);
		*(((char*)addr)+i)=0;
	}
	printf("\nend of data\n\n");
	for(int i=0;i<bufLen;i++) 
	{
		printf("%c",*((char*)addr+(nWr-1)*bufLen+i) & 0xff);
	}
}

clean_qp:
	printf("ibv_destroy_qp() \n");
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	printf("ibv_destroy_cq() \n");
	ibv_destroy_cq(cq);

clean_mr:
	printf("ibv_dereg_mr() \n");
	ibv_dereg_mr(mr);

clean_pd:
	printf("ibv_dealloc_pd() \n");
	ibv_dealloc_pd(pd);

clean_comp_channel:
	printf("ibv_destroy_comp_channel() \n");
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	printf("ibv_close_device() \n");
	ibv_close_device(context);

clean_buffer:
	printf("free() \n");
	free(buf_gth);

_end:
	return -1;
}
