int nWr		= 8,
	bufLen	= 2048,
    nIter   = 1;

void displayProfilingResult(struct timespec *end, struct timespec *start, 
int n, int l, int i)
{
	double 	dt_ms = (end->tv_nsec-start->tv_nsec)/1000000.0 + (end->tv_sec-start->tv_sec)*1000.0;
	double 	gbps = (double) i * n * l * 8 / dt_ms * 1e3 / 1e9;
	double 	pps  = (double) i * n  / dt_ms;
	double 	fps  = (double) i * 1000 / dt_ms ;

	printf("BW %.1f Gbits/s, %.1fk pps, %.1f fps using single GPU core. (%d images) dt_ms %f ms\n",
	 							gbps, pps, fps, i,dt_ms);
}

void select_cpu()
{
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(1, &set);
	CPU_SET(3, &set);
	CPU_SET(5, &set);
	CPU_SET(7, &set);
	// CPU_SET(9, &set);
	// CPU_SET(11, &set);
	sched_setaffinity(0, sizeof(set), &set);
	unsigned cpu = sched_getcpu();
	printf("thread using cpu %d\n", cpu);
}