/*
 * 
 * 
 * gcc ud_postrecv.c -o ud_postrecv -libverbs
 * 
 * 
 */

#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <infiniband/verbs.h>

#include "ud.h"
#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)

CUdevice 			cuDevice;
CUcontext 			cuContext;
CUdeviceptr 		d_A;


void init_nvidia_cuda_gpu_device(int alloc_size) 
{
	
	int gpuId 		= 1;
	printf("init_nvidia_cuda_gpu_device devID %d\n", gpuId);

	cuInit(0);

	int deviceCount = 0;
	cuDeviceGetCount(&deviceCount);
	
	
	printf("cudaSetDeviceFlags\n");
	cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);

	/* pick up device with zero ordinal (default, or devID) */
	cuDeviceGet(&cuDevice, gpuId);
	cudaCheckErrors("cuDeviceGet");

	char name[128];
	cuDeviceGetName(name, sizeof(name), gpuId);
	printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);

	/* Create context */
	cuContext=NULL;
	printf("cuCtxCreate\n");
	cuCtxCreate(&cuContext, CU_CTX_MAP_HOST, cuDevice);
	
	printf("cuCtxSetCurrent\n");
	cuCtxSetCurrent(cuContext);
	cudaCheckErrors("cuCtxSetCurrent");
	
	cuMemAlloc(&d_A, alloc_size) ; 
	cudaCheckErrors("cuMemAlloc");
 
	// unsigned int flag = 1;
    // ASSERTDRV(cuPointerSetAttribute(&flag, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS, d_A));	
	printf("RDMA capable GPU, allocated GPU memory at %p\n", d_A);
}


char displayBuf[4096];

int main(int argc, char**argv)
{
int gpu		    = atoi(argv[1]);
int d		    = atoi(argv[2]);
int nWr 	    = atoi(argv[3]);
int bufLen 	    = atoi(argv[4]);
int nIter 	    = atoi(argv[5]);
int rr 		    = atoi(argv[6]);
// int remote_qpn	= atoi(argv[7]);

int memorySize 	= nWr*bufLen;

select_cpu();

struct timespec startTime, endTime;

struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
struct ibv_comp_channel *channel=NULL;
struct ibv_pd *pd;
struct ibv_mr *mr,*mr_gth; 
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size	= sysconf(_SC_PAGESIZE);
int ib_port 	= 1 ;

//dest memory allocation backed by HUGE PAGES
char devname[128]="/dev/hugepages/rashpadata";

int fd = open(devname, O_RDWR|O_CREAT, 0755);
if (fd < 0) 
{
	perror("/dev/hugepages/rashpadata Open failed");
	exit(1);
}
void* addr ; 

//GPU MEMORY
if (gpu==1) 
{
	init_nvidia_cuda_gpu_device(memorySize);
	addr = (void*) d_A;
}

//HUGE PAGES
if (gpu==0)
{
	addr = mmap(	NULL,
					memorySize ,
					PROT_READ|PROT_WRITE,
					MAP_SHARED|MAP_HUGETLB,
					fd,
					0);

	if (addr == MAP_FAILED)
	{
		perror("mmap ");
		printf("of %d error\n",nWr * bufLen );
		unlink("/dev/hugepages/rashpadata");
		exit(1);
	}	
}

//CUDA PINNED MEMORY
if(gpu==2)
{
	cudaHostAlloc(&addr,memorySize,0);
	cudaCheckErrors("cudaHostAlloc");
}
//clear dataset
if (gpu!=1) 
	for (int i=0; i<nWr; i++)
		for (int j=0; j<bufLen; j++)
			*((char*)addr+i*bufLen+j) = 0;

asm volatile("" ::: "memory");        // Compiler barrier
asm volatile("mfence" ::: "memory");  // Hardware barrier
asm volatile("sfence" ::: "memory");  // Hardware barrier
asm volatile("lfence" ::: "memory");  // Hardware barrier


//40 bytes extra bytes memory allocation
// char *buf_gth  = memalign(page_size, 40);

//get devices list
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return -1;
	}

ib_dev = dev_list[d];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) 
{
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	goto clean_buffer;
}
printf("using device %s\n",ibv_get_device_name(ib_dev));
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) 
{
	fprintf(stderr, "Couldn't allocate PD\n");
	goto clean_comp_channel;
}

//Memory Region
printf("ibv_reg_mr\n");
mr 		= ibv_reg_mr(pd, addr, memorySize, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
if (!mr) 
{
	fprintf(stderr, "Couldn't register MR\n");
	goto clean_pd;
}
//Completion Queue
printf("ibv_create_cq\n");
cq = ibv_create_cq(context, 16, NULL,NULL, 0);
if (!cq) 
{
	fprintf(stderr, "Couldn't create CQ\n");
	goto clean_mr;
}

//Queue Pair
printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 1;
qp_init_attr.cap.max_send_sge = 1;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 16;
qp_init_attr.cap.max_recv_sge = 1;
//qp_init_attr.qp_type = IBV_QPT_UD; 
qp_init_attr.qp_type = IBV_QPT_UC; 

printf("ibv_create_qp\n");
qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}

struct ibv_qp_attr attr = {
			.qp_state        = IBV_QPS_INIT,
			.pkey_index      = 0,
			.port_num        = ib_port,
			.qp_access_flags = IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE//|IBV_ACCESS_REMOTE_ATOMIC 
		};

printf("ibv_modify_qp to INIT\n");
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
          IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}
union ibv_gid gid;

//use show_gids to determine index
if (ibv_query_gid(context, ib_port,3, &gid)) 
{
	fprintf(stderr, "ibv_query_gid error\n");
	return -1;
}
printf("%x%x%x%x%x%x%x%x%x%x\n",gid.raw[0],gid.raw[1],gid.raw[2],gid.raw[3],gid.raw[4],gid.raw[5],gid.raw[6],gid.raw[7],gid.raw[8],gid.raw[9]);
printf("%x%x%x%x%x%x\n",gid.raw[10],gid.raw[11],gid.raw[12],gid.raw[13],gid.raw[14],gid.raw[15]);

//Mandatory
gid.raw[15] = 13; //SOURCE ADDRESS
//Modify QP to RTR

printf("ibv_modify_qp to RTR\n");
memset(&attr, 0, sizeof(attr));
attr.qp_state		        = IBV_QPS_RTR;
attr.path_mtu		        = IBV_MTU_4096 ;
// attr.dest_qp_num	        = remote_qpn; //not required
attr.rq_psn		            = 1234;

attr.ah_attr.dlid	        = 0; 
attr.ah_attr.sl		        = 0;
attr.ah_attr.src_path_bits  = 0;
attr.ah_attr.port_num	    = 1;//do not change
attr.ah_attr.is_global	    = 1;  //1 below is valid
attr.ah_attr.grh.hop_limit 	= 0xff;
attr.ah_attr.grh.dgid 		= gid;
attr.ah_attr.grh.sgid_index = 3;

if (ibv_modify_qp(qp, &attr,    IBV_QP_STATE    |
                                IBV_QP_AV       |
                                IBV_QP_PATH_MTU |
                                IBV_QP_DEST_QPN | // IBV_QP_MIN_RNR_TIMER|IBV_QP_MAX_DEST_RD_ATOMIC
                                IBV_QP_RQ_PSN)) 
{
	fprintf(stderr, "Failed to modify QP to RTR\n");
	return -1;
}


printf("UC waiting for remote WRITE data at buffer %p qp_num %d rkey %x mem %d\n",
		mr->addr, qp->qp_num,mr->rkey,memorySize);

//wait for transfer completion (WRITE WITH IMMEDIATE)
// struct ibv_wc 		wc[1];
// struct ibv_sge		sg ;
// struct ibv_recv_wr	wr,*bad_wr;
// wr.wr_id     	= 1;
// wr.next 		= NULL;
// wr.sg_list   	= &sg;
// wr.num_sge 		= 1;
// sg.length  	= 0;
// sg.lkey	  	= 0;
// sg.addr 	= 0;
	
// ibv_post_recv(qp, &wr, &bad_wr);
// while(ibv_poll_cq(cq, 1, wc)==0);
// printf("imm data %d\n",ntohl(wc[0].imm_data));


//wait for event
struct ibv_async_event event;
printf("in, ibv_get_async_event\n");
int ret = ibv_get_async_event(context, &event);
printf("after, ibv_get_async_event\n");
if (ret) 
{
	fprintf(stderr, "Error, ibv_get_async_event() failed\n");
	return -1;
}
ibv_ack_async_event(&event);
// getchar();
for (int i=0;i<nWr;i++)
{
	puts(addr + i*bufLen);
	//puts(addr);
	// for (int j=0; j<32; j++)
	// 	printf("%x %x %x %x ",	
	// 					*((char*)(((uint64_t)mr->addr)+j)) & 0xff,
	// 					*((char*)(((uint64_t)mr->addr)+j+  bufLen)) & 0xff,
	// 					*((char*)(((uint64_t)mr->addr)+j+2*bufLen)) & 0xff,
	// 					*((char*)(((uint64_t)mr->addr)+j+3*bufLen)) & 0xff
	// 					) ;

	// asm volatile("" ::: "memory");        // Compiler barrier
	// asm volatile("mfence" ::: "memory");  // Hardware barrier
	// asm volatile("sfence" ::: "memory");  // Hardware barrier
	// asm volatile("lfence" ::: "memory");  // Hardware barrier
	// usleep(1000000);
	// printf("\n");
}


clean_qp:
	printf("ibv_destroy_qp() \n");
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	printf("ibv_destroy_cq() \n");
	ibv_destroy_cq(cq);

clean_mr:
	printf("ibv_dereg_mr() \n");
	ibv_dereg_mr(mr);

clean_pd:
	printf("ibv_dealloc_pd() \n");
	ibv_dealloc_pd(pd);

clean_comp_channel:
	printf("ibv_destroy_comp_channel() \n");
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	printf("ibv_close_device() \n");
	ibv_close_device(context);

clean_buffer:
	printf("free() \n");
	// free(buf_gth);

_end:
	printf("end... \n");
	return -1;
}
