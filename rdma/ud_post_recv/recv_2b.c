/*
 * 
 * 
 * gcc ud_postrecv.c -o ud_postrecv -libverbs
 * 
 * 
 */

#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <infiniband/verbs.h>

#include "ud.h"
#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)

CUdevice 			cuDevice;
CUcontext 			cuContext;
CUdeviceptr 		d_A;


void init_nvidia_cuda_gpu_device(int alloc_size) 
{
	
	int gpuId 		= 1;
	printf("init_nvidia_cuda_gpu_device devID %d\n", gpuId);

	cuInit(0);

	int deviceCount = 0;
	cuDeviceGetCount(&deviceCount);
	
	
	printf("cudaSetDeviceFlags\n");
	cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);

	/* pick up device with zero ordinal (default, or devID) */
	cuDeviceGet(&cuDevice, gpuId);
	cudaCheckErrors("cuDeviceGet");

	char name[128];
	cuDeviceGetName(name, sizeof(name), gpuId);
	printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);

	/* Create context */
	cuContext=NULL;
	printf("cuCtxCreate\n");
	cuCtxCreate(&cuContext, CU_CTX_MAP_HOST, cuDevice);
	
	printf("cuCtxSetCurrent\n");
	cuCtxSetCurrent(cuContext);
	cudaCheckErrors("cuCtxSetCurrent");


	
	cuMemAlloc(&d_A, alloc_size) ; 
	cudaCheckErrors("cuMemAlloc");
 
	// unsigned int flag = 1;
    // ASSERTDRV(cuPointerSetAttribute(&flag, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS, d_A));	
	printf("RDMA capable GPU, allocated GPU memory at %p\n", d_A);
}


char displayBuf[4096];

int main(int argc, char**argv)
{
int gpu		= atoi(argv[1]);
int d		= atoi(argv[2]);
int nWr 	= atoi(argv[3]);
int bufLen 	= atoi(argv[4]);
int nIter 	= atoi(argv[5]);
int rr 		= atoi(argv[6]);

int memorySize = nWr*bufLen;

select_cpu();

struct timespec startTime, endTime;

struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
struct ibv_comp_channel *channel=NULL;
struct ibv_pd *pd;
struct ibv_mr *mr,*mr_gth; 
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size=sysconf(_SC_PAGESIZE);
int ib_port = 1 ;

//dest memory allocation backed by HUGE PAGES
char devname[128]="/dev/hugepages/rashpadata";

int fd = open(devname, O_RDWR|O_CREAT, 0755);
if (fd < 0) 
{
	perror("/dev/hugepages/rashpadata Open failed");
	exit(1);
}
void* addr ; 

//GPU MEMORY
if (gpu==1) 
{
	init_nvidia_cuda_gpu_device(memorySize);
	addr = (void*) d_A;
}

//HUGE PAGES
if (gpu==0)
{
	addr = mmap(	NULL,
					memorySize ,
					PROT_READ|PROT_WRITE,
					MAP_SHARED|MAP_HUGETLB,
					fd,
					0);

	if (addr == MAP_FAILED)
	{
		perror("mmap ");
		printf("of %d error\n",nWr * bufLen );
		unlink("/dev/hugepages/rashpadata");
		exit(1);
	}	
}

//CUDA PINNED MEMORY
if(gpu==2)
{
	cudaHostAlloc(&addr,memorySize,0);
	cudaCheckErrors("cudaHostAlloc");
}
//clear dataset
if (gpu!=1) 
	for (int i=0; i<nWr; i++)
		*((char*)addr+i*bufLen) = 0;

//40 bytes extra bytes memory allocation
char *buf_gth  = memalign(page_size, 40);

//get devices list
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return -1;
	}

ib_dev = dev_list[d];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) 
{
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	goto clean_buffer;
}
printf("using device %s\n",ibv_get_device_name(ib_dev));
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) 
{
	fprintf(stderr, "Couldn't allocate PD\n");
	goto clean_comp_channel;
}

//Memory Region
mr 		= ibv_reg_mr(pd, addr,  memorySize, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr_gth 	= ibv_reg_mr(pd, buf_gth, 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);

if (!mr) 
{
	fprintf(stderr, "Couldn't register MR\n");
	goto clean_pd;
}
//Completion Queue
printf("ibv_create_cq\n");
cq = ibv_create_cq(context, nWr, NULL,NULL, 0);
if (!cq) 
{
	fprintf(stderr, "Couldn't create CQ\n");
	goto clean_mr;
}

//Queue Pair
printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 1;
qp_init_attr.cap.max_send_sge = 1;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 2*nWr;
qp_init_attr.cap.max_recv_sge = 2;
qp_init_attr.qp_type = IBV_QPT_UD; 

qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}
printf("ibv_create_qp qnum %d\n",qp->qp_num);
struct ibv_qp_attr attr = {
			.qp_state        = IBV_QPS_INIT,
			.pkey_index      = 0,
			.port_num        = ib_port,
			.qkey            = 0xCAFECAFE 
			//.qp_access_flags = 0 //IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE 

		};

printf("ibv_modify_qp to INIT\n");
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY //IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}

//Modify QP to RTR
printf("ibv_modify_qp to RTR\n");
memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) 
{
	fprintf(stderr, "Failed to modify QP to RTR\n");
	return -1;
}

//Post Recv Work Request
struct ibv_sge		*sg = (struct ibv_sge*)		memalign(page_size,2*nWr*sizeof(struct ibv_sge));
struct ibv_recv_wr	*wr	= (struct ibv_recv_wr*)	memalign(page_size,nWr*sizeof(struct ibv_recv_wr)),*bad_wr;
struct ibv_wc 		*wc	= (struct ibv_wc*)		memalign(page_size,nWr*sizeof(struct ibv_wc));
memset(sg,0,2*nWr*sizeof(struct ibv_sge));
memset(wr,0,nWr*sizeof(struct ibv_recv_wr));

for (int i = 0 ; i < nWr ; i++)
{
	wr[i].wr_id     	= i;
	wr[i].next 			= (i == (nWr -1) || i == (nWr/2 -1)) ? NULL : &wr[i+1];
	wr[i].sg_list   	= &(sg[2*i]);
	wr[i].num_sge 		= 2;
	sg[2*i].length  	= 40;
	sg[2*i].lkey	  	= mr_gth->lkey;
	sg[2*i].addr 		= (uint64_t) buf_gth;

	sg[2*i+1].length 	= bufLen;
	sg[2*i+1].lkey		= mr->lkey;
	sg[2*i+1].addr 		= ((uint64_t) addr) + i * bufLen;
}



for (int r=0;r<rr;r++)
{
usleep(rand()%10000);
int bank=0,psn;
bank=0;

//int nTest=rand()%(nWr/2-4);

//1) post nWr/2 requests
if (ibv_post_recv(qp, wr, &bad_wr)) 
{
	printf("error ibv_post_recv 1\n");
	return -1;
}

//2) poll 1
int p=0,n=0;
do 
{
	n = ibv_poll_cq(cq, 1, wc);
} while (n<1);	
	
clock_gettime(CLOCK_MONOTONIC, &startTime);
psn = ntohl(wc[0].imm_data) % nWr;
printf("start %d\n",r);

	
if (psn) 
{
	printf("psn %d\n",psn);
	//3) post nWr/2-psn
	if (ibv_post_recv(qp, &wr[psn], &bad_wr))
	{
		printf("error ibv_post_recv 21\n");
		return -1;
	}
	//4)post nWr/2
	if (ibv_post_recv(qp, wr, &bad_wr))
	{
		printf("error ibv_post_recv 22\n");
		return -1;
	}
	
	if(psn>nWr/2)
	{
		if (ibv_post_recv(qp, wr, &bad_wr))
		{
			printf("error ibv_post_recv 22\n");
			return -1;
		}
		p=0;
		do 
		{
			n = ibv_poll_cq(cq, nWr/2 - p, wc);
			p += n;
		} while (p < (nWr/2));
	}
	//5)poll nWr - psn
	p=0;
	do 
	{
		n = ibv_poll_cq(cq, nWr-psn - p, wc);
		p += n;
	} while (p < (nWr-psn));

	if(psn>nWr/2)
	{
		p=0;
		do 
		{
			n = ibv_poll_cq(cq, nWr/2 - p, wc);
			p += n;
		} while (p < (nWr/2));
	}
}

//main loop, post (2*nIter-1)*nWr/2
//start with i=1 because 1 wr has alredy been sent
for (int i=1;i<2*nIter;i++)
{
	if (ibv_post_recv(qp, bank ? &wr[0] : &wr[nWr/2], &bad_wr))
	{
		printf("error ibv_post_recv 2\n");
		return -1;
	}

	p=0;
	do 
	{
		n = ibv_poll_cq(cq, nWr/2-p, wc);
		p +=n;
	} while (p < nWr/2);
	
	//testing
	int onum,inum;
	onum = inum;	
	inum = ntohl(wc[n-1].imm_data);
	if ((inum-onum) > (nWr/2) 
	|| (inum % (nWr/2)!=0)) 
		printf("lost image %d\n",inum);
	

	bank = !bank;

#define TEST 9997
	if ((gpu!=1) && (i==(TEST+1))) 
	{
		printf(((((char*)addr)+(0)*bufLen)));
		printf("\n");
		printf(((((char*)addr)+(nWr/2-1)*bufLen)));
		printf("\n");
	}
	if ((gpu==1) && (i==(TEST+1)))
	{
		printf("cudaMemcpy\n");
		cudaMemcpy(
				(void*) displayBuf, 
				(void*) d_A, 
				bufLen, 
				cudaMemcpyDeviceToHost);
		printf(displayBuf);
	}
		
	if ((gpu!=1) && (i==(TEST+2)))  
	{			
		printf(((((char*)addr)+(nWr/2)*bufLen)));
		printf("\n");
		printf(((((char*)addr)+(nWr-1)*bufLen)));
		printf("\n");
	}
	if ((gpu!=1) && (i==(TEST+3)))
	{
		printf(((((char*)addr)+(0)*bufLen)));
		printf("\n");
		printf(((((char*)addr)+(nWr/2-1)*bufLen)));
		printf("\n");
	}
	if ((gpu!=1) && (i==(TEST+4)))  
	{			
		printf(((((char*)addr)+(nWr/2)*bufLen)));
		printf("\n");
		printf(((((char*)addr)+(nWr-1)*bufLen)));
		printf("\n");
	}
	

	//prepare descriptors : not required, as we are transfering in same region and clear data
	if (gpu!=1)
		for (int ii = (bank ? 0 : nWr/2) ; ii < (bank ? nWr/2 : nWr) ; ii++)
			* (char*) (((uint64_t) addr) + ii*bufLen) = 0;
		else
			{
				 cudaMemset((void*)d_A,0,bufLen) ;
			}
			
	
}

clock_gettime(CLOCK_MONOTONIC, &endTime);
displayProfilingResult(&endTime, &startTime, nWr, bufLen, nIter);

//purge NIC
int ll=0,l;
do 
{
	l=ibv_poll_cq(cq,nWr, wc);
	ll++;
} while (l>0    || ll<40000);

}//end main loop
if (gpu==1)
	{
		printf("last cudaMemcpy\n");
		cudaMemcpy(
				(void*) displayBuf, 
				(void*) d_A+(nWr/2)*bufLen, 
				bufLen, 
				cudaMemcpyDeviceToHost);
		printf(displayBuf);
	}
clean_qp:
	printf("ibv_destroy_qp() \n");
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	printf("ibv_destroy_cq() \n");
	ibv_destroy_cq(cq);

clean_mr:
	printf("ibv_dereg_mr() \n");
	ibv_dereg_mr(mr);

clean_pd:
	printf("ibv_dealloc_pd() \n");
	ibv_dealloc_pd(pd);

clean_comp_channel:
	printf("ibv_destroy_comp_channel() \n");
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	printf("ibv_close_device() \n");
	ibv_close_device(context);

clean_buffer:
	printf("free() \n");
	free(buf_gth);

_end:
	printf("end... \n");
	return -1;
}
