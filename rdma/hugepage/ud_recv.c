#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h> 	
#include <infiniband/verbs.h>

int main()
{
struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
	
struct ibv_comp_channel *channel;

struct ibv_pd *pd;
struct ibv_mr *mr,*mr_gth;
struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size=sysconf(_SC_PAGESIZE);
int size       = 512; //less than MTU
int N_DGRAM	   = 10;
int send_flags = IBV_SEND_SIGNALED;
int rx_depth   = 500;
//FIXME
int ib_port = 1 ;


printf("allocate huge pages\n");
//memory allocation
int fd = open("/hugepages/rdmadata", O_RDWR|O_CREAT, 0777);
if (fd < 0) {
	perror("Open failed");
	exit(1);
	}
int _size=16*1024*1024;
char*buf = mmap(0, _size,  PROT_WRITE, MAP_SHARED|MAP_HUGETLB, fd, 0);
if (buf == MAP_FAILED) {
	perror("mmap");
	unlink("rdmadata");
	exit(1);
	}
memset(buf,  '/', _size);
buf[100]=0;
puts("buf before");			
puts(buf + 0);	


//memory allocation
char *gth= memalign(page_size, 40);

		
//getchar();
//get device
dev_list = ibv_get_device_list(NULL);
	if (!dev_list) {
		perror("Failed to get IB devices list");
		return -1;
	}

ib_dev = dev_list[0];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) {
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	goto clean_buffer;
}
printf("using device %s\n",ibv_get_device_name(ib_dev));

channel = ibv_create_comp_channel(context);
if (!channel) {
	fprintf(stderr, "Error, ibv_create_comp_channel() failed\n");
	return -1;
}
	
//Protection Domain
pd = ibv_alloc_pd(context);
if (!pd) {
	fprintf(stderr, "Couldn't allocate PD\n");
	goto clean_comp_channel;
}

//Memory Region
mr_gth = ibv_reg_mr(pd, gth, 40, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
mr = ibv_reg_mr(pd, buf, N_DGRAM*size, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE);
if (!mr || !mr_gth) {
	fprintf(stderr, "Couldn't register MR\n");
	goto clean_pd;
}
//Completion Queue
printf("ibv_create_cq %p %p %d\n",context,channel,rx_depth);

cq = ibv_create_cq(context, 100, NULL,channel, 0);
	if (!cq) {
		fprintf(stderr, "Couldn't create CQ\n");
		goto clean_mr;
	}
printf("ibv_create_cq done \n");
	

//Queue Pair

printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = 10;
qp_init_attr.cap.max_send_sge = 10;
//qp_init_attr.cap.max_inline_data = 100;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 10;
qp_init_attr.cap.max_recv_sge = 10;
qp_init_attr.qp_type = IBV_QPT_UD; 
printf("ibv_create_qp\n");
qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) {
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}
printf("ibv_create_qp %d\n",qp->qp_num);

struct ibv_qp_attr attr = {
			.qp_state        = IBV_QPS_INIT,
			.pkey_index      = 0,
			.port_num        = ib_port, //this is ib_port
			.qkey            = 0xCAFECAFE //remote_qpn the other side
			//.qp_access_flags = 0 //IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE 

		};
printf("ibv_modify_qp to INIT\n");
 
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY //IBV_QP_ACCESS_FLAGS
		  )) {
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}

//Modify QP to RTR
printf("ibv_modify_qp to RTR\n");

memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) {
		fprintf(stderr, "Failed to modify QP to RTR\n");
		return -1;
	}
int i ;

//Post Recv Work Request
struct ibv_sge list [4];
list[0].addr	= (uintptr_t) gth;
list[0].length  = 40;
list[0].lkey	= mr_gth->lkey;

list[1].length  = size;
list[1].lkey	= mr->lkey;



//~ list[2].addr	= (uintptr_t) buf3;
//~ list[2].length  = size+40;
//~ list[2].lkey	= mr3->lkey;
//~ list[3].addr	= (uintptr_t) buf2;
//~ list[3].length  = size;
//~ list[3].lkey	= mr2->lkey;

struct ibv_recv_wr wr = {
		.wr_id	    = 0x1234,
		.sg_list    = &list[0],
		.num_sge    = 2,
	};
//~ struct ibv_recv_wr wr2 = {
		//~ .wr_id	    = 0x1235,
		//~ .sg_list    = &list[2],
		//~ .num_sge    = 2,
	//~ };
	


struct ibv_recv_wr *bad_wr;
for(i=0;i<N_DGRAM;i++) {
	printf("ibv_post_recv\n");
	list[1].addr	= (uintptr_t) buf+i*(size);
	if (ibv_post_recv(qp, &wr, &bad_wr)) return -1;
//if (ibv_post_recv(qp, &wr2, &bad_wr)) return -1;
}		
			


puts("getchar");			
getchar();

puts("\nbuf0");
buf[100]=0;
puts(buf);	
puts("\nbuf1");
buf[size+100]=0;
puts(buf+size);	

puts("\nbuf+5*size");
buf[5*size+100]=0;
puts(buf +5*size);				
		 
clean_qp:
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	ibv_destroy_cq(cq);

clean_mr:
	ibv_dereg_mr(mr);

clean_pd:
	ibv_dealloc_pd(pd);

clean_comp_channel:
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	ibv_close_device(context);

clean_buffer:
	//free(buf);

_end:
	munmap(buf,_size);
	close(fd);
	return -1;

}
