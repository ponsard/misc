
/*
 * 
 * 
 * 
 * gcc ud_send.c -o ud_send -libverbs
 * 
 * 
 */


#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <malloc.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>

#include <infiniband/verbs.h>
#include "../ud_post_recv/ud.h"


void displayProfilingResult(struct timespec *end, struct timespec *start, int n, int l, int i)
{
	double 	dt_ms = (end->tv_nsec-start->tv_nsec)/1000000.0 + (end->tv_sec-start->tv_sec)*1000.0;
	double 	gbps = (double) i * n * l * 8 / dt_ms * 1e3 / 1e9;
	double 	pps  = (double) i * n  / dt_ms;
	double 	fps  = (double) n / dt_ms * 1000;

	printf("\nBW %.1f Gbits/s, %.1fk pps, %.1f fps using single GPU core. (%d images) dt_ms %f ms\n\n",
	 							gbps, pps, fps, i,dt_ms);
}


void select_cpu()
{
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(1, &set);
	CPU_SET(3, &set);
	CPU_SET(5, &set);
	CPU_SET(7, &set);
	CPU_SET(9, &set);
	CPU_SET(11, &set);
	sched_setaffinity(0, sizeof(set), &set);
	unsigned cpu = sched_getcpu();
	printf("thread using cpu %d\n", cpu);
}




int main(int argc, char**argv) 
{
struct timespec startTime, endTime;

int d			= atoi(argv[1]);
int nWr 		= atoi(argv[2]);
int bufLen 		= atoi(argv[3]);
int nIter 		= atoi(argv[4]);
int remote_qpn	= atoi(argv[5]);



struct ibv_device   **dev_list;
struct ibv_device	*ib_dev;
	
struct ibv_comp_channel *channel=NULL;

struct ibv_pd *pd;

struct ibv_mr *mr,*mr2,*mr3,*mr4;

struct ibv_cq *cq;
struct ibv_qp *qp;
struct ibv_qp_init_attr qp_init_attr;
struct ibv_context	*context;

int page_size=sysconf(_SC_PAGESIZE);
int send_flags = IBV_SEND_SIGNALED;
int ib_port=1;

select_cpu();
printf("ud send v0.0\n");

//memory allocation
char *buf =  memalign(page_size, bufLen*nWr );

if (!buf)
{
	fprintf(stderr, "Couldn't allocate work buf.\n");
	exit(0);
}
for(int i=0;i<nWr*bufLen;i++) 
{
	*(buf+i) = 'a' + (i % 26);
}
for(int i=0;i<bufLen;i++) 
{
	*(((char*)buf+(nWr-1)*bufLen)+i)='A' + (i % 26);
}

//get device
dev_list = ibv_get_device_list(NULL);
if (!dev_list) 
{
	perror("Failed to get IB devices list");
	return -1;
}

ib_dev = dev_list[d];
if (!ib_dev) {
	fprintf(stderr, "No IB devices found\n");
	return -1;
}

//create context
context = ibv_open_device(ib_dev);
if (!context) {
	fprintf(stderr, "Couldn't get context for %s\n",
		ibv_get_device_name(ib_dev));
	exit(0);
}
printf("using device %s\n",ibv_get_device_name(ib_dev));

struct ibv_port_attr port_info = {};
if (ibv_query_port(context, ib_port, &port_info)) 
{
	fprintf(stderr, "Unable to query port info for port %d\n", ib_port);
}

int mtu = 1 << (port_info.active_mtu + 7);
printf("MTU (%d)\n", mtu);
if (bufLen > mtu)
{
	fprintf(stderr, "Requested size larger than port MTU (%d)\n", mtu);
}
	
//Protection Domain
printf("ibv_alloc_pd\n");
pd = ibv_alloc_pd(context);
if (!pd) {
	fprintf(stderr, "Couldn't allocate PD\n");
	exit(0);
}

//Memory Region
printf("ibv_reg_mr\n");

mr  = ibv_reg_mr(pd, buf,  bufLen*nWr , IBV_ACCESS_LOCAL_WRITE);
if (!mr) 
{
	fprintf(stderr, "Couldn't register MR\n");
	exit(0);
}

//Completion Queue
printf("ibv_create_cq %p %p %d\n",context,channel,nWr);
cq = ibv_create_cq(context, nWr, NULL,channel, 0);
if (!cq) 
{
	fprintf(stderr, "Couldn't create CQ\n");
	exit(0);
}
	

//Queue Pair
printf("creating QP\n");
memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
qp_init_attr.send_cq = cq;
qp_init_attr.recv_cq = cq;
qp_init_attr.cap.max_send_wr  = nWr;
qp_init_attr.cap.max_send_sge = 1;
//~ qp_init_attr.cap.max_inline_data = 512;
//qp_init_attr.srq = NULL;
qp_init_attr.cap.max_recv_wr  = 1;
qp_init_attr.cap.max_recv_sge = 1;
qp_init_attr.qp_type = IBV_QPT_UD; 
printf("ibv_create_qp\n");
qp = ibv_create_qp(pd,&qp_init_attr);
if (!qp) 
{
	fprintf(stderr, "---------Error, ibv_create_qp() failed\n");
	return -1;
}

struct ibv_qp_attr attr = {
	.qp_state        = IBV_QPS_INIT,
	.pkey_index      = 0,
	.port_num        = ib_port, 
	.qkey            = 0xCAFECAFE
};

printf("ibv_modify_qp to INIT\n");
if (ibv_modify_qp(qp, &attr,
		  IBV_QP_STATE              |
		  IBV_QP_PKEY_INDEX         |
		  IBV_QP_PORT 				| 
		  IBV_QP_QKEY 
		  )) 
{
	fprintf(stderr, ">>>>>>>>>Failed to modify QP to INIT\n");
	return -1;
}

memset(&attr, 0, sizeof(attr));
attr.qp_state		= IBV_QPS_RTR;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE)) 
{
	fprintf(stderr, "Failed to modify QP to RTR\n");
	return -1;
}

//Modify QP to RTS
printf("ibv_modify_qp to RTS\n");
memset(&attr, 0, sizeof(attr));
attr.qp_state	= IBV_QPS_RTS;
attr.sq_psn	    = 1234;
if (ibv_modify_qp(qp, &attr, IBV_QP_STATE|IBV_QP_SQ_PSN))
{
	fprintf(stderr, "Failed to modify QP to RTS\n");
	return -1;
}


//Create ah
struct ibv_ah		*ah; 
struct ibv_ah_attr ah_attr;
memset(&ah_attr, 0, sizeof(ah_attr));
union ibv_gid gid;

if (ibv_query_gid(context, ib_port, 1, &gid)) 
{
	fprintf(stderr, "ibv_query_gid\n");
	return -1;
}
gid.raw[10] = 255;
gid.raw[11] = 255;
gid.raw[12] = 192;
gid.raw[13] = 168;
gid.raw[14] = 3;
gid.raw[15] = 13;

ah_attr.is_global     	= 1;
ah_attr.grh.dgid 		= gid; 
ah_attr.grh.sgid_index 	= 0;
ah_attr.grh.hop_limit 	= 0xFF;
ah_attr.grh.traffic_class = 1;
ah_attr.dlid          	= 0;
ah_attr.sl            	= 0;
ah_attr.src_path_bits 	= 0;
ah_attr.port_num      	= ib_port;

ah = ibv_create_ah(pd, &ah_attr);
if (!ah) 
{
	fprintf(stderr, "Error, ibv_create_ah() failed\n");
	return -1;
}

printf("ibv_post_send nWR %d to %d\n", nWr, remote_qpn);

struct ibv_sge* sg  	= (struct ibv_sge*)     memalign(page_size, nWr*sizeof(struct ibv_sge));
struct ibv_send_wr* wr  = (struct ibv_send_wr*) memalign(page_size, nWr*sizeof(struct ibv_send_wr)),*bad_wr;
memset(sg, 0, nWr*sizeof(struct ibv_sge));
memset(wr, 0, nWr*sizeof(struct ibv_send_wr));

for(int i=0; i<nWr; i++) 
{
	wr[i].wr_id      			= i;
	wr[i].opcode     			= IBV_WR_SEND_WITH_IMM;
	wr[i].send_flags 			= IBV_SEND_SIGNALED; //try UNSIGNALED qp_init_attr.sq_sig_all=0
	wr[i].wr.ud.remote_qpn 		= remote_qpn;
	wr[i].wr.ud.ah          	= ah;
	wr[i].wr.ud.remote_qkey 	= 0xCAFECAFE;
	wr[i].next 					= (i == (nWr -1)) ? NULL : &wr[i+1];
	wr[i].imm_data				= htonl(0x123456+i);
	wr[i].sg_list    			= &sg[i];
	wr[i].num_sge    			= 1;
	sg[i].length  				= bufLen;
	sg[i].lkey	  				= mr->lkey;
	sg[i].addr 					= (uint64_t) buf+i*bufLen;
}

struct ibv_wc wc[nWr];
//
clock_gettime(CLOCK_MONOTONIC, &startTime);
for(int j=0;j<nIter;j++)
{
	
	if (j%1000==0) printf("ibv_post_send %d\n",j);
	
	for (int i=0; i<nWr; i++)
		wr[i].imm_data = htonl(0x123456+i+j*nWr);

	if (ibv_post_send(qp, wr, &bad_wr)) 
	{
		fprintf(stderr, "Error, ibv_post_send() failed\n");
		return -1;
	}

	int num_comp=0;
	do
	{
		num_comp += ibv_poll_cq(cq, nWr-num_comp, wc);
	} while (num_comp<nWr);	
		
	if (wc[0].status != IBV_WC_SUCCESS)  
	{
		fprintf(stderr, "Failed status %s (%d) for wr_id %d\n", 
		ibv_wc_status_str(wc[0].status),wc[0].status, (int)wc[0].wr_id);
		return 0;
	}
}
clock_gettime(CLOCK_MONOTONIC, &endTime);
displayProfilingResult(&endTime, &startTime, nWr, bufLen, nIter);

clean_qp:
	if (ibv_destroy_qp(qp)) {
	fprintf(stderr, "Error, ibv_destroy_qp() failed\n");
	return -1;
}

clean_cq:
	ibv_destroy_cq(cq);

clean_mr:
	ibv_dereg_mr(mr);

clean_pd:
	ibv_dealloc_pd(pd);

clean_comp_channel:
	if (channel) ibv_destroy_comp_channel(channel);

clean_device:
	ibv_close_device(context);

clean_buffer:
	free(buf);

_end:
	return -1;

}
