#include <json.h>
#include <stdio.h>

int main() {
  /*Creating a json object*/
  json_object * jobj = json_object_new_object();

  /*Creating a json double*/
  json_object *jdouble = json_object_new_double(3.1419275555555555);
  json_object *jint = json_object_new_int(123456);

	json_object *my_array = json_object_new_array();
	json_object_array_add(my_array, json_object_new_int(1));
	json_object_array_add(my_array, json_object_new_int(2));
	json_object_array_add(my_array, json_object_new_int(3));

  /*Form the json object*/
  json_object_object_add(jobj,"PI", jdouble);
  json_object_object_add(jobj,"int1", jint);
  json_object_object_add(jobj,"array", my_array);
  
	json_object *baz_obj = json_object_new_string("fark");
	json_object_get(baz_obj);
	//json_object_object_add(jobj, "baz", baz_obj);
	//json_object_object_del(my_object, "baz");
  /*Now printing the json object*/
  printf ("The json object created: %sn",json_object_to_json_string(jobj));

}
