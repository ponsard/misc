/*
 * 
 * 
 
nvcc -ccbin g++-6 -m64  -I /media/ponsard/NVMESSD/NVIDIA_CUDA-8.0_Samples/common/inc \
-lcuda -libverbs \
-gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_60,code=compute_60   \
-o cudabw ./cudabw.cu -I ../../gdrcopy/ -L ../../gdrcopy/ -lgdrapi -Xcompiler -fopenmp

OMP_NUM_THREADS=4 LD_LIBRARY_PATH=../../gdrcopy/ ./cudabw


cp ../../gdrcopy/gdrdrv/gdrdrv.ko /tmp
sudo insmod /tmp/gdrdrv.ko 
sudo dmesg check MAJOR
sudo mknod /dev/gdrdrv c 237 0
sudo chmod a+rw /dev/gdrdrv
*
* 
* 
*/ 
 
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <assert.h>
#include <cstdint>
#include <gdrapi.h>
#include <cuda.h>

#include <cuda_runtime_api.h>
extern "C" int omp_get_num_threads();



// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", 
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

void profileCopiesH2D(float     *h_a, 
                   float        *d, 
                   unsigned int  n,
                   const char         *desc)
{
  // events for timing
  cudaEvent_t startEvent, stopEvent; 

  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );
  checkCuda( cudaMemcpy(d, h_a, n, cudaMemcpyHostToDevice) );
  checkCuda( cudaEventRecord(startEvent, 0) );
  checkCuda( cudaMemcpy(d, h_a, n, cudaMemcpyHostToDevice) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  float time;
  checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  printf("%s, %f\n", desc, 8 * n * 1e-6 / time);

  // clean up events
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
}

void profileCopiesD2H(float     *h_a, 
                   float        *d, 
                   unsigned int  n,
                   const char         *desc)
{
  cudaEvent_t startEvent, stopEvent; 

  checkCuda( cudaEventCreate(&startEvent) );
  checkCuda( cudaEventCreate(&stopEvent) );
  checkCuda( cudaMemcpy(h_a, d, n, cudaMemcpyDeviceToHost) );
  checkCuda( cudaEventRecord(startEvent, 0) );
  checkCuda( cudaMemcpy(h_a, d, n, cudaMemcpyDeviceToHost) );
  checkCuda( cudaEventRecord(stopEvent, 0) );
  checkCuda( cudaEventSynchronize(stopEvent) );
  float time;
  checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
  printf("%s, %f\n",desc, 8 * n * 1e-6 / time);

  // clean up events
  checkCuda( cudaEventDestroy(startEvent) );
  checkCuda( cudaEventDestroy(stopEvent) );
}

void mymcpy(float *dst, float *src, unsigned int n)
{
#pragma omp parallel
{
#pragma omp for
    for (int k=0; k < n/4; k ++)
      dst[k] = src[k];
}
}

void profileCopiesH2H(float *dst, float *src, unsigned int  n, const char *msg)
{
  struct timespec endTime,  startTime;
  
  memcpy(dst, src, n);
  clock_gettime(CLOCK_MONOTONIC, &startTime);
  memcpy(dst, src, n);
  clock_gettime(CLOCK_MONOTONIC, &endTime);
  double 	dt_ms = (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
  double 	gbps = (double) n * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
  printf("%s, %.2f\n",msg, gbps);
}

void profileCopiesOMPH2H(float *dst, float *src, unsigned int  n, const char *msg)
{
  struct timespec endTime,  startTime;
int k=-1;
  #pragma omp parallel 
  {
#pragma omp master
{
    k = omp_get_num_threads();
    //printf ("Number of Threads requested = %i\n",k);
      }
  }

k = 0;
#pragma omp parallel
#pragma omp atomic 
  k++;
  //printf ("Number of Threads counted = %i\n",k);


  mymcpy(dst, src, n);
  clock_gettime(CLOCK_MONOTONIC, &startTime);
  mymcpy(dst, src, n);
  clock_gettime(CLOCK_MONOTONIC, &endTime);
  double 	dt_ms = (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
  double 	gbps = (double) n * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
  printf("%s, %.2f\n",msg, gbps);
}

void cpy_to_bar(float *dst, float *src, unsigned int  n, const char *msg)
{
  struct timespec endTime,  startTime;

  gdr_copy_to_bar(dst, src, n);
  int 
  numthreads = omp_get_num_threads();
  clock_gettime(CLOCK_MONOTONIC, &startTime);

clock_gettime(CLOCK_MONOTONIC, &startTime);
#pragma omp parallel
{
#pragma omp for
  for (int k=0; k < numthreads; k ++)
  gdr_copy_to_bar(dst, src, n);
}
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	double dt_ms = (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps = (double) n * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	printf("%s, %.2f\n",msg, gbps);
}
void cpy_from_bar(float *dst, float *src, unsigned int  n, const char *msg)
{
  struct timespec endTime,  startTime;
	gdr_copy_from_bar(dst, src, n);
	
  
  int 
    numthreads = omp_get_num_threads();
  
  clock_gettime(CLOCK_MONOTONIC, &startTime);
#pragma omp parallel
{
#pragma omp for
    for (int k=0; k < numthreads; k ++)
      //dst[k] = src[k];
      gdr_copy_from_bar(dst+k*n/numthreads, src+k*n/numthreads, n/numthreads);
}

  
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	double dt_ms = (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps = (double) n * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	printf("%s, %.2f\n", msg, gbps);
}


int main()
{
int devId = 1;
cudaSetDevice(devId);
cudaDeviceProp prop;
checkCuda( cudaGetDeviceProperties(&prop,devId) );
//printf("GPU Device: %s\n", prop.name);
	  
	  
// for (size_t nElements = 1; nElements < (64*1024*1024UL); nElements *= 2) {
	int nElements = 64*1024*1024;
  size_t bytes = nElements * sizeof(float);
  //printf("#Transfer size :%d %f(MB)\n", bytes, (float)bytes / (1024 * 1024));
	int	page_size	= sysconf(_SC_PAGESIZE);


  int fd1 = open("/dev/hugepages/rashpadata1", O_RDWR|O_CREAT, 0755);
  int fd2 = open("/dev/hugepages/rashpadata2", O_RDWR|O_CREAT, 0755);
  if (fd1 < 0||fd2<0) 
  {
    perror("/dev/hugepages/rashpadata Open failed");
    exit(1);
  }
  void* huge_addr1 = mmap(	NULL,
            bytes ,
            PROT_READ|PROT_WRITE,
            MAP_SHARED|MAP_HUGETLB,
            fd1,
            0);

  void* huge_addr2 = mmap(	NULL,
              bytes ,
              PROT_READ|PROT_WRITE,
              MAP_SHARED|MAP_HUGETLB,
              fd2,
              0);
  
  if (huge_addr1 == MAP_FAILED   || huge_addr2 == MAP_FAILED)
  {
    perror("mmap ");
    printf("of %d error\n",bytes);
    unlink("/dev/hugepages/rashpadata");
    exit(1);
  }	

  //cudaHostRegister();
	cudaHostRegister((void*)huge_addr1, bytes, cudaHostRegisterDefault);		
	cudaHostRegister((void*)huge_addr2, bytes, cudaHostRegisterDefault);		

  // host arrays
  float *h_aPageable, *h_bPageable;   
  float *h_aPinned, *h_bPinned;

  // device array
  float *d_a;
  //CUdeviceptr d_A;
  // allocate and initialize
  h_aPageable = (float*)memalign(page_size, bytes);                    // host pageable
  h_bPageable = (float*)memalign(page_size, bytes);     
  
  // host pageable
  checkCuda( cudaMallocHost((void**)&h_aPinned, bytes) ); // host pinned
  checkCuda( cudaMallocHost((void**)&h_bPinned, bytes) ); // host pinned
  checkCuda( cudaMalloc((void**)&d_a, bytes) );           // device
  for (int i = 0; i < nElements; ++i) {
    h_aPageable[i] = i;  
    h_aPinned[i] = 2*i;
    ((float*)huge_addr1)[i] = 3*i;
  }


  memset(h_bPageable, 0, bytes);
  memset(h_bPinned, 0, bytes);
  
  profileCopiesH2H(h_bPageable, h_aPageable, bytes, "h_aPageable -> h_bPageable");
	profileCopiesH2H(h_bPinned, h_aPageable, bytes,   "h_aPageable -> h_bPinned");
	profileCopiesH2H(h_bPageable, h_aPinned, bytes,   "h_aPinned -> h_bPageable");
	profileCopiesH2H(h_bPinned, h_aPinned, bytes,     "h_aPinned -> h_bPinned");
	profileCopiesH2H((float*)huge_addr1, (float*)huge_addr2, bytes,     "huge -> huge");

	// profileCopiesOMPH2H(h_bPinned, h_aPageable, bytes, "OMP h_aPageable");
	// profileCopiesOMPH2H(h_bPinned, h_aPinned, bytes,   "OMP h_aPinned ");

  // cpy_to_bar(h_bPinned, h_aPinned, bytes,    "gdrcpy_to_BAR");
  // cpy_from_bar(h_bPinned, h_aPinned, bytes,  "gdrcpy_from_bar");

  
  profileCopiesH2D(h_aPageable, d_a, bytes,  "HPageable -> D");
  profileCopiesD2H(h_bPageable, d_a, bytes,  "D -> HPageable");
  profileCopiesH2D(h_aPinned, d_a, bytes,    "HPinned -> D");
  profileCopiesD2H(h_bPinned, d_a, bytes,    "D -> HPinned");
  profileCopiesH2D((float*)huge_addr1, d_a, bytes,     "huge -> D");
  profileCopiesD2H((float*)huge_addr2, d_a, bytes,     "D -> huge");

  for (int i = 0; i < nElements; ++i) {
    if (h_aPageable[i] !=  h_bPageable[i]) printf("error pageable\n");
    if (h_aPinned[i] != h_aPinned[i]) printf("error pinned\n");
    if (((float*)huge_addr1)[i] != ((float*)huge_addr2)[i]) printf("error huge\n");
  }


  //cudaFree(d_a);
  
  
//   //if (bytes > 128*1024*1024) bytes = 128*1024*1024;
//   float *init_buf = NULL;
//   init_buf = (float *)malloc(bytes);
//   CUdeviceptr d_A0;
//   cuMemAlloc(&d_A0, bytes);
//   //std::cout << "device ptr: " << std::hex << d_A0 << std::dec << std::endl;
//   unsigned int flag = 1;
//   cuPointerSetAttribute(&flag, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS, d_A0);
//   gdr_t g = gdr_open();
//   gdr_mh_t mh;
//   gdr_pin_buffer(g, d_A0, bytes, 0, 0, &mh);

//   void *bar_ptr  = NULL;
//   gdr_map(g, mh, &bar_ptr, bytes);
//   //std::cout << "bar_ptr: " << bar_ptr << std::endl;
// if (bar_ptr==0) exit(0);
//   gdr_info_t info;
//   gdr_get_info(g, mh, &info);
//   // std::cout << "info.va: " << std::hex << info.va << std::dec << std::endl;
//   // std::cout << "info.mapped_size: " << info.mapped_size << std::endl;
//   // std::cout << "info.page_size: " << info.page_size << std::endl;

//   int off = info.va - d_A0;
//   //std::cout << "page offset: " << off << std::endl;

//   float *buf_ptr = (float *)((char *)bar_ptr + off);
//   // std::cout << "user-space pointer:" << buf_ptr << std::endl;

//   // printf("gdr_copy_to_bar\n");

//   cpy_to_bar(buf_ptr, init_buf, bytes,    "gdrcpy_to_BAR");
//   cpy_from_bar(init_buf, buf_ptr, bytes,  "gdrcpy_from_bar");

 

// gdr_unmap(g, mh, bar_ptr, bytes);
// gdr_unpin_buffer(g, mh);
// gdr_close(g);
// cuMemFree(d_A0);

  // cudaFreeHost(h_aPinned);
  // cudaFreeHost(h_bPinned);
  // free(h_aPageable);
  // free(h_bPageable);

  // free(huge_addr1);
  // free(huge_addr2);
  // }

  return 0;
}
