// SPDX-License-Identifier: GPL-2.0
/*
 * hugepage-mmap:
 *
 * Example of using huge page memory in a user application using the mmap
 * system call.  Before running this application, make sure that the
 * administrator has mounted the hugetlbfs filesystem (on some directory
 * like /mnt) using the command mount -t hugetlbfs nodev /mnt. In this
 * example, the app is requesting memory of size 256MB that is backed by
 * huge pages.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>


//2GB of 2MB
//echo 4096 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages


#define MAP_HUGE_2MB    (21 << MAP_HUGE_SHIFT)
#define MAP_HUGE_1GB    (30 << MAP_HUGE_SHIFT)

#define FILE_NAME "/dev/hugepages/hugepagefile"


//scisoft14 only 2M page 

//~ #define LENGTH (1UL*1024*1024)
#define LENGTH (8UL*1024*1024*1024)
//~ #define LENGTH (4UL*1024*1024)

//~ #define LENGTH (100UL*100*512*512*3)



#define PROTECTION (PROT_READ | PROT_WRITE)
#define ADDR (void *)(0x0UL)
#define FLAGS (MAP_PRIVATE|MAP_HUGETLB |MAP_HUGE_2MB|MAP_ANONYMOUS )

static void check_bytes(char *addr)
{
	printf("First hex is %x\n", *((unsigned int *)addr));
}

static void write_bytes(char *addr)
{
	unsigned long i;

	for (i = 0; i < LENGTH; i++)
		*(addr + i) = (char)i;
}

static int read_bytes(char *addr)
{
	unsigned long i;

	check_bytes(addr);
	for (i = 0; i < LENGTH; i++)
		if (*(addr + i) != (char)i) {
			printf("Mismatch at %lu\n", i);
			return 1;
		}
	return 0;
}

int main(void)
{
	void *addr,*addr2;
	int fd, ret;

	fd = open(FILE_NAME, O_CREAT | O_RDWR, 0755);
	if (fd < 0) {
		perror("Open failed");
		exit(1);
	}
	addr = (char*)mmap((void *)(0x0UL), LENGTH,  
	PROT_READ |PROT_WRITE, (MAP_PRIVATE|MAP_HUGETLB |MAP_HUGE_2MB ), fd, 0);
	addr2 = (char*)mmap((void *)(0x0UL), LENGTH,  
	PROT_READ |PROT_WRITE, (MAP_PRIVATE|MAP_HUGETLB |MAP_HUGE_2MB ), fd, 0);
	//~ addr = (char*)mmap((void *)(0x0UL), LENGTH/2,  PROT_READ |PROT_WRITE, (MAP_PRIVATE|MAP_HUGETLB |MAP_HUGE_2MB|MAP_ANONYMOUS ), 0, 0);
	//~ addr = (char*)mmap(0, LENGTH, PROTECTION, FLAGS, 0, 0);
	if (addr == MAP_FAILED || addr2 == MAP_FAILED) {
		perror("mmap");
		unlink(FILE_NAME);
		exit(1);
	}
printf(" v1.0 len %ld %f GB %p\n", LENGTH,(float) LENGTH/1024/1024/1024, addr);
getchar();

	printf("Returned address is %p\n", addr);
	check_bytes(addr);
	printf("write test\n");
	write_bytes(addr);
	printf("read test\n");
	ret = read_bytes(addr);
getchar();
	munmap(addr, LENGTH);
	close(fd);
	unlink(FILE_NAME);

	return ret;
}
