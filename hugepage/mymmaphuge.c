#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>


#define totalSize			1024*1024*1024
#define FILE_NAME "/dev/hugepages/hugepagefile"


int main()
{
	int fd = open(FILE_NAME, O_CREAT | O_RDWR, 0755);
	if (fd < 0) {
		perror("Open failed");
		exit(1);
	}
	
	
	char* h_aPinned = (char*)mmap(
	(void *)(0x0UL), 
	totalSize/2,  
	PROT_READ |PROT_WRITE, 
	MAP_SHARED,
	 fd, 0);
	 
	if (h_aPinned == MAP_FAILED) {
		perror("mmap1 failed");
		exit(1);
	}
	
	printf("%p %d\n",h_aPinned);

return 0;

}
