#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h> 
#include <fcntl.h> 

#include <sys/mman.h>
#include <linux/memfd.h>

main()
{
	int len=1024*1024*1024;
	int fd = memfd_create("myrdmamem", MFD_HUGETLB |MFD_HUGE_2MB);
	if(fd==-1)errExit("memfd_create");
	if (ftruncate(fd, len) == -1)
		   errExit("truncate");
		   
    getchar();
}
