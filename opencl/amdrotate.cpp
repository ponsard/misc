/*
 * 
 * g++ rotate.cpp -o rotate -lOpenCL -lm -I /media/ponsard/NVMESSD/AMDAPPSDK-3.0/include/SDKUtil/ -I /media/ponsard/NVMESSD/AMDAPPSDK-3.0/include
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <math.h>
#include <CL/opencl.h>
#include "CL/cl_ext.h" //AMD
 
															
#include "CLUtil.hpp"
#define SAMPLE_VERSION "AMD-APP-SDK-v3.0.130.4"
using namespace appsdk;

/*
 * event raised by romulu at the end of data transfer
 * 
 * 
 */
void isrUSR1(int x)
{
	//clEnqueueWriteSignalAMD (queue, remote_buffer, value, 0, 0, NULL, &event);
}

 
int main( int argc, char* argv[] )
{
	unsigned int width	= 512,
				 height	= 512;
				 
	signal(SIGUSR1,isrUSR1);
				 
	CLCommandArgs   *sampleArgs=new CLCommandArgs();   	/* CLCommand argument class */
	SDKDeviceInfo 	deviceInfo; 						/* SDKDeviceInfo class object */
	cl_device_id    *devices;      						/* CL device list */

	sampleArgs->initialize();			 
	if(sampleArgs->parseCommandLine(argc, argv))
	{
		return SDK_FAILURE;
	}

	cl_int status = 0;
	cl_device_type dType;

	if(sampleArgs->deviceType.compare("cpu") == 0)
	{
		dType = CL_DEVICE_TYPE_CPU;
	}
	else //deviceType = "gpu"
	{
		dType = CL_DEVICE_TYPE_GPU;
		if(sampleArgs->isThereGPU() == false)
		{
			std::cout << "GPU not found. Falling back to CPU device" << std::endl;
			dType = CL_DEVICE_TYPE_CPU;
		}
	}

    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */

    cl_platform_id platform = NULL;
    int retValue = getPlatform(platform, sampleArgs->platformId,sampleArgs->isPlatformEnabled());
    CHECK_ERROR(retValue, SDK_SUCCESS, "getPlatform() failed");

    // Display available devices.
    retValue = displayDevices(platform, dType);
    CHECK_ERROR(retValue, SDK_SUCCESS, "displayDevices() failed");
	/*
     * If we could find our platform, use it. Otherwise use just available platform.
     */

    cl_context_properties cps[3] =
    {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties)platform,
        0
    };

    cl_context context = clCreateContextFromType(
                  cps,
                  dType,
                  NULL,
                  NULL,
                  &status);

    CHECK_OPENCL_ERROR(status, "clCreateContextFromType failed.");

    // getting device on which to run the sample
    status = getDevices(context, &devices, sampleArgs->deviceId,
                        sampleArgs->isDeviceIdEnabled());
    CHECK_ERROR(status, SDK_SUCCESS, "getDevices() failed");

    // Get Device specific Information, Set device info of given cl_device_id
    retValue = deviceInfo.setDeviceInfo(devices[sampleArgs->deviceId]);
    CHECK_ERROR(retValue, SDK_SUCCESS, "SDKDeviceInfo::setDeviceInfo() failed");

	
	// The block is to move the declaration of prop closer to its use
	cl_command_queue_properties *props=NULL;// [] = {0,0,0};//{CL_QUEUE_PROFILING_ENABLE,0,0,0};
	
	cl_command_queue queue = clCreateCommandQueueWithProperties(context,devices[sampleArgs->deviceId],props,&status);
	CHECK_ERROR(status, 0, "clCreateCommandQueue failed.");

    // Set Persistent memory only for AMD platform
    cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
    if(sampleArgs->isAmdPlatform())
        // To achieve best performance, use persistent memory together with
        // clEnqueueMapBuffer (instead of clEnqeueRead/Write).
        // At the same time, in general, the best performance is the function
        // of access pattern and size of the buffer.
    {
        inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD;
    }


	// Host input vectors
	char *h_input;
	// Host output vector
	char *h_output;
	// Device input buffers
	cl_mem d_input;
	// Device output buffer
	cl_mem d_output;

	// Size, in bytes, of each vector
	size_t bytes = 3*width*height;

	// Allocate memory for each vector on host
	h_input  = (char*)malloc(bytes);
	h_output = (char*)malloc(bytes);
	memset(h_output,0,bytes);

	// Initialize vectors on host
	int f=open("lena.data", O_RDWR);
	int n=read(f,h_input,bytes);
	printf("read %d bytes\n",n);
	close(f);

	clEnqueueMakeBuffersResidentAMD_fn clEnqueueMakeBuffersResidentAMD;
	clEnqueueWaitSignalAMD_fn clEnqueueWaitSignalAMD;
	clEnqueueMakeBuffersResidentAMD = (clEnqueueMakeBuffersResidentAMD_fn)clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueMakeBuffersResidentAMD");
	if (clEnqueueMakeBuffersResidentAMD == NULL) {
		fprintf (stderr, "Could not load clEnqueueMakeBuffersResidentAMD");
	}
  
	clEnqueueWaitSignalAMD = (clEnqueueWaitSignalAMD_fn)clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWaitSignalAMD");
	if (clEnqueueWaitSignalAMD == NULL) {
		fprintf (stderr, "Could not load clEnqueueWaitSignalAMD");
	}
	cl_int err;
	cl_program program;
	buildProgramData buildData;
    buildData.kernelName = std::string("rotate.cl");
    buildData.devices = devices;
    buildData.deviceId = sampleArgs->deviceId;
    buildData.flagsStr = std::string("");
    if(sampleArgs->isLoadBinaryEnabled())
    {
        buildData.binaryName = std::string(sampleArgs->loadBinary.c_str());
    }

    if(sampleArgs->isComplierFlagsSpecified())
    {
        buildData.flagsFileName = std::string(sampleArgs->flags.c_str());
    }

    retValue = buildOpenCLProgram(program, context, buildData);
    CHECK_ERROR(retValue, SDK_SUCCESS, "buildOpenCLProgram() failed");
	
	size_t extensionSize;
	clGetDeviceInfo(devices[sampleArgs->deviceId], CL_DEVICE_EXTENSIONS, 0, NULL, &extensionSize);

    char* ExtensionName = (char*)malloc(extensionSize);
    clGetDeviceInfo(devices[sampleArgs->deviceId], CL_DEVICE_EXTENSIONS, extensionSize, ExtensionName, NULL);
	printf("ExtensionName %s\n",ExtensionName);
	
	
	
	  //clEnqueueWaitSignalAMD (queue, local_buffer, value, 0, NULL, &event);

	
	
	// Create the compute kernel in the program we wish to run
	//cl_kernel kernel = clCreateKernel(program, "myrotate", &err);
	cl_kernel kernel = clCreateKernel(program, "myflip", &err);
	if (err) printf("ERROR clCreateKernel %d\n",err);

	// Create the input and output arrays in device memory for our calculation
	//d_input  = clCreateBuffer(context, CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, bytes,(void *) h_input, NULL);
	d_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, bytes, NULL, NULL);

	// Write our data set into the input array in device memory
	//err = clEnqueueWriteBuffer(queue, d_input, CL_TRUE, 0, bytes, h_input, 0, NULL, NULL);
	//printf("clEnqueueWriteBuffer %d\n",err);  CL_MEM_EXTERNAL_PHYSICAL_AMD
	
    d_input = clCreateBuffer (context, CL_MEM_BUS_ADDRESSABLE_AMD, bytes, NULL, &err);
	if(err)printf("clCreateBuffer with CL_MEM_BUS_ADDRESSABLE_AMD error\n");
	cl_bus_address_amd addr[1]; memset(addr, 0, sizeof(cl_bus_address_amd));
	cl_mem buff[2];
	buff[0]=d_input;//(cl_mem)((uint64_t)d_input & ~0xfff);
	printf("rounded %p d_input %p\n",buff[0],d_input);
	err =	clEnqueueMakeBuffersResidentAMD (queue, 1, buff, CL_TRUE, addr , 0, 0, 0 );
	if(err)printf(" EEROR clEnqueueMakeBuffersResidentAMD%d\n",err);
	
	
	err=0;
	cl_bus_address_amd busAddr_;
	cl_mem busAddressableBuff_ = clCreateBuffer(context, CL_MEM_BUS_ADDRESSABLE_AMD, 1*1024, 0, &err);
	if(err)printf(" errot CL_MEM_BUS_ADDRESSABLE_AMD %d\n",err);

	// Get physical address
	err = clEnqueueMakeBuffersResidentAMD(queue, 0, &busAddressableBuff_, true, &busAddr_, 0,0,0);
	if(err)printf(" EEROR2 clEnqueueMakeBuffersResidentAMD%d\n",err);
	//#define CL_OUT_OF_RESOURCES                         -5
  	printf("rounded %p d_input %p\n",buff[0],d_input);

	// Set the arguments to our compute kernel
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_input);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_output);
	err |= clSetKernelArg(kernel, 2, sizeof(unsigned int), &width);
	err |= clSetKernelArg(kernel, 3, sizeof(unsigned int), &height);
	printf("clSetKernelArg %d\n",err);


  //clEnqueueWaitSignalAMD (queue, local_buffer, value, 0, NULL, &event);


	size_t       globalThreads[3]={512,512,0};   /**< global NDRange */
    size_t       localThreads[3]={16,16,0}; 
	// Execute the kernel over the entire range of the data set 
	err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, globalThreads, localThreads, 0, NULL, NULL);
	printf("clEnqueueNDRangeKernel %d\n",err);

	// Wait for the command queue to get serviced before reading back results
	clFinish(queue);

	// Read the results from the device
	clEnqueueReadBuffer(queue, d_output, CL_TRUE, 0, bytes, h_output, 0, NULL, NULL );

	f=open("rotatedlena.data", O_RDWR|O_CREAT|O_TRUNC,S_IRWXU);
	write(f,h_output,bytes);
	close(f);
	// release OpenCL resources
	clReleaseMemObject(d_input);
	clReleaseMemObject(d_output);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);

	//release host memory
	free(h_input);
	free(h_output);

	return 0;
}
