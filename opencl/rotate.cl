/*
 * 
 * 
 * 
 */											
#pragma OPENCL EXTENSION cl_amd_printf : enable	
#pragma OPENCL EXTENSION cl_intel_printf :enable
								                      		
__kernel void myrotate(__global char *inputImage,                    
                       __global char *outputImage,                   
                       int w,                      
                       int h)                    	
{                                      		
	int i = get_global_id(0),ii;
	int j = get_global_id(1),jj; 
	int N = get_global_id(2); 
	//size_t id = get_global_id(0)*get_global_size(1) + get_global_id(1);
	ii = h-1 - j;
	jj = i;

	//printf("i %d j %d \n",i,j); 

	if (i < w && j < h && ii < h && jj < w) {
		outputImage[3*(ii+h*jj)  ]= inputImage[3*(i+w*j)  ]; 
		outputImage[3*(ii+h*jj)+1]= inputImage[3*(i+w*j)+1] ; 
		outputImage[3*(ii+h*jj)+2]= 0xff;//inputImage[3*(i+w*j)+2]; 
	}
}

__kernel void myflip(__global char *inputImage,                    
                       __global char *outputImage,                   
                       int w,                      
                       int h)                    	
{                                      		
	int i = get_global_id(0),ii;
	int j = get_global_id(1),jj; 
	//size_t id = get_global_id(0)*get_global_size(1) + get_global_id(1);
	ii = i;
	jj = h-1 - j;

	//printf("i %d j %d \n",i,j); 

	if (i < w && j < h && ii < h && jj < w) {
		outputImage[3*(ii+h*jj)  ]= inputImage[3*(i+w*j)  ]; 
		outputImage[3*(ii+h*jj)+1]= inputImage[3*(i+w*j)+1] ; 
		outputImage[3*(ii+h*jj)+2]= inputImage[3*(i+w*j)+2]; 
	}
}
