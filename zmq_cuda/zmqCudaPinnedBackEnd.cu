//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//gcc csub.c -o csub -lzmq

#include "zhelpers.h"


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <unistd.h>

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    printf("CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}




__global__ void rightRotation(char *inputImage,char *outputImage,int Width,int Height)
{   unsigned int i = blockIdx.x*blockDim.x + threadIdx.x,ii;
    unsigned int j = blockIdx.y*blockDim.y + threadIdx.y,jj;
    // (Height-1 - j,Width-1 - i)    <=  (i,j)
    ii = j; 
    jj = Width-1 - i;

    if (i < Width && j < Height && ii < Height && jj < Width) {
        outputImage[3*(ii+jj* Height)+0] = inputImage[3*(i+j* Width)+0];
        outputImage[3*(ii+jj* Height)+1] = inputImage[3*(i+j* Width)+1];
        outputImage[3*(ii+jj* Height)+2] = inputImage[3*(i+j* Width)+2];
        }
}



//~ char url[]="tcp://192.168.3.13:5557";
char url[]="pgm://192.168.3.14:5557";
//~ char url[]="epgm://enp216s0f1;*:5557";

int width=512,height=512;
float *stats;
void *context;
void *subscriber;
cudaStream_t 			s0,s1,s2,s3;

void profileDataTransfer(
				   char        *h_a, 
                   char        *h_b, 
                   char        *d, 
                   char        *d2, 
                   unsigned long int  bytes,
                   int nImg, int iter,int s
                   )
{
	dim3 dimBlock(512,1, 1);
	dim3 dimGrid (1,512, 1);
	int start = 1;
	// events for timing
	cudaEvent_t startEvent, stopEvent; 
	checkCuda( cudaEventCreate(&startEvent) );
	checkCuda( cudaEventCreate(&stopEvent) );

	for (int k=0; k<iter; k++) 
	{
		
		int l = 0;
		do
		{
			l +=  zmq_recv (subscriber,h_a + k*bytes*nImg + l, bytes*nImg - l, 0);
			//~ printf("read %d bytes : \n", l);
			if (start) 
			{
					checkCuda( cudaEventRecord(startEvent, 0) );
					start = 0;
			}
		}
		while (l < bytes*nImg);

		checkCuda( cudaMemcpyAsync(	d,
									h_a + k*bytes*nImg,
									bytes*nImg, 
									cudaMemcpyHostToDevice) );
		for(int i=0;i<nImg;i++) 
		{
			rightRotation <<< dimGrid, dimBlock >>>( d+i*bytes, d2+i*bytes, width,height );
		}
		
		checkCuda( cudaMemcpyAsync(	h_b + k*bytes*nImg, 
									d2,
									bytes*nImg, 
									cudaMemcpyDeviceToHost) );
	}
	checkCuda( cudaEventRecord(stopEvent, 0) );
	checkCuda( cudaEventSynchronize(stopEvent) );

	float time;
	checkCuda( cudaEventElapsedTime(&time, startEvent, stopEvent) );
	stats[s] = (float) 8.0*bytes*(iter-1)*nImg/time*1000.0/1024.0/1024.0/1024.0;
	printf("N->H H->D K D->H bandwidth (Gb/s): %f\n", stats[s]);
	checkCuda( cudaEventDestroy(startEvent) );
	checkCuda( cudaEventDestroy(stopEvent) );
}

int main()
{

	int devId = 1;
	checkCuda(cudaSetDevice(devId));

	// zmq  Socket 
    printf ("Collecting images from python publisher %s\n",url);
    context = zmq_ctx_new ();
    subscriber = zmq_socket (context, ZMQ_SUBSCRIBE);
    int rc = zmq_connect (subscriber, url);
    assert (rc == 0);

    rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, NULL, 0); //ZMQ_DONTWAIT
    assert (rc == 0);
	//~ checkCuda(cudaStreamCreate(&s0));
	//~ checkCuda(cudaStreamCreate(&s1));
	//~ checkCuda(cudaStreamCreate(&s2));
	//~ checkCuda(cudaStreamCreate(&s3));

#define STP 2
	stats = (float*)malloc(STP*sizeof(float));
	
	for(int stp=1; stp<STP;stp++) 
	{
		int bytes 	= 3*512*512;
		int N 		= 100;
		int iter 	= 100;

		char *h_aPinned, *h_bPinned;
		char *d_a, *d_b;

		// N*iter images in pinned memory
		checkCuda( cudaMallocHost((void**)&h_aPinned, (unsigned long int)bytes*N*iter) ); // host pinned
		//~ printf("h_aPinned %p size %ld\n",(char*)h_aPinned,(unsigned long int)bytes*N*iter);

		checkCuda( cudaMallocHost((void**)&h_bPinned, (unsigned long int)bytes*N*iter) );
		//~ printf("h_bPinned %p\n",(char*)h_bPinned);
		
		 // N images in device memory
		checkCuda( cudaMalloc((void**)&d_a, bytes*N) );
		checkCuda( cudaMalloc((void**)&d_b, bytes*N) );

		//~ memset(h_bPinned, 0, bytes*N);
		memset(stats,0,STP*sizeof(float));

		// output device info and transfer size
		cudaDeviceProp prop;
		checkCuda( cudaGetDeviceProperties(&prop, devId) );

		printf("\nDevice: %s  step %d iter %d ready to accept data\n", prop.name, stp, iter);

		//~ int f0 = open("/tmp/lena100.data", O_RDWR|O_CREAT,S_IRWXU);
		//~ read(f0,h_aPinned,bytes*N);
		//~ close(f0);
		 
		profileDataTransfer(h_aPinned, h_bPinned, d_a, d_b,bytes,N,iter,stp);
		
		cudaFree(d_a);
		cudaFree(d_b);
		cudaFreeHost(h_aPinned);
		
		int f2 = open("/tmp/stat.data", O_RDWR|O_CREAT,S_IRWXU);
		write(f2, stats, STP*sizeof(float));	
		close(f2);

		int f1 = open("/tmp/res.data", O_RDWR|O_CREAT,S_IRWXU);
		unsigned long int 	res = 0,
							totalSize = (unsigned long int) N*iter*bytes;
		do
		{
			res += write(f1, h_bPinned + res, totalSize-res);	
			printf("saving in /tmp/res.data... %ld h_bPinned %p\n",res, h_bPinned);
		}
		while (res < totalSize);
		
		close(f1);
		cudaFreeHost(h_aPinned);
		zmq_close (subscriber);
		zmq_ctx_destroy (context);
	}
	return 0;
}


