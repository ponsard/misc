//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//gcc csub.c -o csub -lzmq

#include "zhelpers.h"

#define bLEN 		4096
char buffer[bLEN];







int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("Collecting images from python publisher\n");
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    int rc = zmq_connect (subscriber, "tcp://192.168.3.14:5557");
    assert (rc == 0);

    rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, NULL, 0); //ZMQ_DONTWAIT
    assert (rc == 0);
    
	//~ puts("setsockopt");
	int l = 0;
	do
    {
        l =  zmq_recv (subscriber,buffer, bLEN,0);
		printf("recv %s len %d\n",buffer,l);
		puts(buffer);
    }
    while (l>0);
    printf ("get all data\n");

    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}
