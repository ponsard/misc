#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */


struct SharedData {
    const char *name;
    int 	descriptor;
    char 	*buf;
    size_t 	nbytes;
};

void shared_open(struct SharedData *data, const char *name, size_t nbytes) {
    int d = shm_open(name, O_RDONLY, 0777);
    if (d != -1) {
        void *mmio = mmap(NULL, nbytes, PROT_READ, MAP_SHARED, d, 0 );
        data->name = name;
        data->descriptor = d;
        data->buf = (char*)mmio;
        data->nbytes = nbytes;
    } else {
		printf("cantopen2\n");
        data->descriptor = -1;
    }
}

void shared_create(struct SharedData *data, const char *name, void *bytes, size_t nbytes) {
    int d = shm_open(name, O_CREAT | O_TRUNC | O_RDWR, 0600 );
    if (d != -1) {
        if (nbytes = write(d, bytes, nbytes)) {
            shared_open(data, name, nbytes);
        }
        printf("cant open\n");
        shm_unlink(name);
    }
}

void shared_close(struct SharedData *data) {
    if (data->descriptor != -1) {
        munmap(data->buf, data->nbytes);
        shm_unlink(data->name);
    }
}


struct SharedData data;
int main()
{
	size_t nbytes=1024;
	
	shared_open(&data, "/mydata", 10);
	printf("data shared : %s\n",data.buf);
		return 0;
}
