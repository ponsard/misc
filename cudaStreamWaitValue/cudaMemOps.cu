/*
 * 
 * 
 
nvcc -ccbin g++-6 -m64  -I /media/ponsard/NVMESSD/NVIDIA_CUDA-8.0_Samples/common/inc \
-lcuda -libverbs \
-gencode arch=compute_50,code=sm_50 -gencode arch=compute_52,code=sm_52 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_60,code=compute_60   \
-o cudaMemOps cudaMemOps.cu


*
* 
* 
*/

#include <stdlib.h>
#include <getopt.h>
#include <memory.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h> 
#include <fcntl.h> 
#include <signal.h>


#include <infiniband/verbs.h>
#include <arpa/inet.h>
#include <malloc.h>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", 
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}



cudaStream_t 		s0,s1;//,s1,s2,s3;
CUdeviceptr 		d_waitFlag;
int					waitFlag = 0;
//volatile int*p;


void isrUSR1(int n)
{
	printf("got sigusr1 v1.1\n");
	//~ cuStreamWriteValue32(s0, d_waitFlag,	2222,	0);
	waitFlag = 2222;
	//p=&waitFlag;
	//*p=2222;
	
	return;
}


__global__ void
vectorAdd(const float *A, const float *B, float *C, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numElements)
    {
        C[i] = A[i] + B[i];
    }
}


static CUdevice 	cuDevice;
//~ static CUcontext 	cuContext;
CUdeviceptr 		d_A;
//~ CUevent 		hEvent;

int devID=1;
int size=1024*1024;



int main()
{
	printf("cuTest v0.2\n");
		signal(SIGUSR1,isrUSR1);

	cuInit(0);//0 only
	
    // Error code to check return values for CUDA calls
    cudaError_t err = cudaSuccess;
    CUresult r;
    
    cuDeviceGet(&cuDevice, devID);

	char name[128];
	cuDeviceGetName(name, 128, devID);
	printf("[pid = %d, dev = %d] device name = [%s]\n", getpid(), cuDevice, name);

int pi = -1;
cuDeviceGetAttribute (&pi,CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS,cuDevice);
printf("CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS %d \n", pi);

checkCuda(cudaStreamCreate(&s0));//, CU_STREAM_DEFAULT));
checkCuda(cudaStreamCreate(&s1));//, CU_STREAM_DEFAULT));

    //~ if (r != cudaSuccess)
    //~ {
        //~ fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        //~ exit(EXIT_FAILURE);
    //~ }

cuMemHostRegister((void*)&waitFlag, 4, CU_MEMHOSTALLOC_DEVICEMAP);
cuMemHostGetDevicePointer(&d_waitFlag, &waitFlag, 0);
//////////////////
    cuStreamWaitValue32( s0, d_waitFlag, 2222, CU_STREAM_WAIT_VALUE_EQ );
	/////////////////////////////
       cudaStreamSynchronize(s0);
printf("was blocked here\n");
//~ volatile * ((int*)&waitFlag) = 0;
    // Print the vector length to be used, and compute its size
    int numElements = 50000;
    size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);

    // Allocate the host input vector A
    float *h_A = (float *)malloc(size);

    // Allocate the host input vector B
    float *h_B = (float *)malloc(size);

    // Allocate the host output vector C
    float *h_C = (float *)malloc(size);

    // Verify that allocations succeeded
    if (h_A == NULL || h_B == NULL || h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
    }

    // Allocate the device input vector A
    float *d_A = NULL;
    err = cudaMalloc((void **)&d_A, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vector B
    float *d_B = NULL;
    err = cudaMalloc((void **)&d_B, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Allocate the device output vector C
    float *d_C = NULL;
    err = cudaMalloc((void **)&d_C, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    printf("Copy input data from the host memory to the CUDA device\n");
    //err = cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    //err = cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector B from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    
    // Launch the Vector Add CUDA Kernel
    int threadsPerBlock = 256;
    int blocksPerGrid =(numElements + threadsPerBlock - 1) / threadsPerBlock;
    printf("v1.1 CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
    vectorAdd<<<blocksPerGrid, threadsPerBlock, 0, s0 >>>(d_A, d_B, d_C, numElements);
    err = cudaGetLastError();

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch vectorAdd kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
 

    // Copy the device result vector in device memory to the host result vector
    // in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    // err = cudaMemcpyAsync(h_C, d_C, size, cudaMemcpyDeviceToHost,s0);
    

    printf("now,was blocked here\n");

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector C from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            exit(EXIT_FAILURE);
        }
    }

    printf("Test PASSED\n");

    // Free device global memory
    err = cudaFree(d_A);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_B);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_C);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Free host memory
    free(h_A);
    free(h_B);
    free(h_C);
	printf("cuTest end...........\n");

}
