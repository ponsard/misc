#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>

#define NUM_THREADS 100
pthread_t threads[NUM_THREADS];



void* thread_code(void*arg)
{
	static int v;
	
	v=*(int*)arg;
	while(1) {
		printf("arg %d tid %x\n",v,pthread_self());
		sleep(1);
	}
}




main()
{
	int nThread=0,rc;
	char c;
	int arg;
	while(1) {
		printf("%d threads, enter cmd %c\n",nThread);
		c=getchar();
		switch(c) {
			case 'n' :
				printf("start new thread\n");
				arg=nThread;
				rc = pthread_create(&threads[nThread++], NULL, thread_code, (void *)&arg);
				break;
			case 'd' :
				printf("delete thread number\n");
				scanf("%d\n",&arg);
				pthread_cancel(threads[arg]);
					break;
		}
		
		}
	
	}
