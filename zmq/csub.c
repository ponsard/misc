//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//gcc csub.c -o csub -lzmq

#include "zhelpers.h"


#include "../../libzmq/include/zmq.h"


#define len 	(512*1024*4*8)
char buffer[len];



int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("Collecting updates from weather server…\n");
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_DISH);
    int rc = zmq_connect (subscriber, "udp://192.168.3.13:5557");
    assert (rc == 0);

    rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, NULL, 0);
    assert (rc == 0);

    int64_t update_nbr=0;
    long total_temp = 0;

    while(1) {

		update_nbr++;
        zmq_recv (subscriber, buffer, len, 0);
        uint64_t hdr = *(uint64_t*)buffer;
		printf("recv  %lu  %lu %ld\n",update_nbr,hdr,update_nbr-hdr );
    }


    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}
