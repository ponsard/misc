//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//gcc csub.c -o csub -lzmq

#include "zhelpers.h"


//#include "../../libzmq/include/zmq.h"

#define COUNT 16*1024




void my_free (void *data, void *hint)
{
    //  We've allocated the buffer using malloc and
    //  at this point we deallocate it using free.
    //free (data);
}

char body_[4096]="hello esrf";

int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("publishing updates \n");
    void *context = zmq_ctx_new ();
    void *radio = zmq_socket (context, ZMQ_RADIO);
        int hwm =15000;
    int rc ;
    // rc= zmq_setsockopt (radio, ZMQ_RCVHWM , &hwm, 4);
     rc = zmq_connect (radio, "udp://192.168.3.13:5557"); //use dish address here 
    assert (rc == 0);

    // rc = zmq_setsockopt (subscriber, ZMQ_RCVHWM , NULL, 0);
    // assert (rc == 0);

    int64_t nbr=0;
    long total_temp = 0;

    //zmq_msg_init_size (&msg, 64);
    // memcpy (zmq_msg_data (&msg), body_, 64);
    // zmq_msg_set_group (&msg, "DATA1");
    // rc = zmq_msg_send (&msg, radio, 0);

    zmq_msg_t msg;
    //zmq_msg_init_size (&msg, 64);
    while (nbr<10000) {
        sprintf(body_,"data %d\n",nbr++);
        //memcpy (zmq_msg_data (&msg), body_, 64);
        zmq_msg_init_data (&msg, body_, 64 , my_free, NULL);
        zmq_msg_set_group (&msg, "DATA1");
        rc = zmq_msg_send (&msg, radio, 0);
        //rc = zmq_send(radio, &msg, strlen(body_), 0);

        if (rc<0) return -1;
		//printf("sent  %d \n", rc);
    }


    zmq_close (radio);
    zmq_ctx_destroy (context);
    return 0;
}
