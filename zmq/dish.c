//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//gcc csub.c -o csub -lzmq

#include "zhelpers.h"
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>


#include "../../libzmq/include/zmq.h"

#define COUNT 16*1024
#define len 	(512*1024*4*8)
char buffer[len];
    int64_t nbr=0;

void* print(void*arg)
{

  while(1)
  {
    sleep(1);
    printf("revfrom num %d / %d\n",nbr,COUNT);
  }
}

int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("Collecting updates server…\n");
    void *context = zmq_ctx_new ();
    void *dish = zmq_socket (context, ZMQ_DISH);
    int hwm =15000;
    int rc ;
    // rc= zmq_setsockopt (dish, ZMQ_RCVHWM , &hwm, 4);

    rc = zmq_bind (dish, "udp://*:5557");
    // int rc = zmq_connect (subscriber, "udp://192.168.3.14:5557");
    assert (rc == 0);

    rc = zmq_join (dish, "DATA1");
    assert (rc == 0);

    long total_temp = 0;
pthread_t tid;
int data;

  pthread_create(&tid,NULL,print,&data);
    while (nbr<COUNT) {

		nbr++;
        int res=zmq_recv (dish, buffer, len, 0);
        //uint64_t hdr = *(uint64_t*)buffer;
		
    }


    zmq_close (dish);
    zmq_ctx_destroy (context);
    return 0;
}
