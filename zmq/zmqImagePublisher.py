"""
publish raw Image data

see notebook

"""
import zmq
import numpy as np


fname='DetectorsData/jungfrau/40keV_4x4ps_e300V_600Hz_5us_beam_000000.dat'
data = np.fromfile(fname,dtype=np.float16,count=-1,sep='')


context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:5556")

socket.send(data)
