
#define _GNU_SOURCE
#include <sched.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <stdlib.h>


#include "udp.h"


struct timespec startTime, endTime;


char buffer[COUNT][LEN]={"hello","esrf"};

struct sockaddr_in 	adr;
int s;
int start = 0;


struct 		mmsghdr msgs[VLEN_SEND];
struct 		iovec iovecs[VLEN_SEND];



int main()
{
	int counter = 0,res;


	select_cpu(SRC);

	s = socket(AF_INET, SOCK_DGRAM, 0);
    if (s ==-1)
    {
    	printf("error creating UDP socket\n");
		return (0);
    }
	adr.sin_family = AF_INET;
	adr.sin_port = htons(PORT);
	adr.sin_addr.s_addr = inet_addr("192.168.3.14");
	if (connect(s, (struct sockaddr *) &adr, sizeof(adr)) == -1) {
	   perror("connect()");
	   exit(EXIT_FAILURE);
	}

//	int optval = 1;
//	setsockopt(s, SOL_SOCKET, SO_NO_CHECK, (void*)&optval, sizeof(optval));
//	setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
//	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	
	memset(msgs, 0, sizeof(msgs));
	for (int i = 0; i < VLEN_SEND; i++) {
	   iovecs[i].iov_len = LEN;
	   iovecs[i].iov_base = buffer[i];
	   msgs[i].msg_hdr.msg_iov    = &iovecs[i];
	   msgs[i].msg_hdr.msg_iovlen = 1;
	}
	clock_gettime(CLOCK_MONOTONIC, &startTime);
	
	for (int j=0; j<COUNT; j += VLEN_SEND) {
		printf("j=%d\n",j);
		for (int i = 0; i < VLEN_SEND; i++) {
		   iovecs[i].iov_base = buffer[j+i];
		}
		int retval = sendmmsg(s, msgs, VLEN_SEND, 0);
		if (retval != VLEN_SEND)
		   perror("sendmmsg()");
	}



	clock_gettime(CLOCK_MONOTONIC, &endTime);
	double dt_ms 	= (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps 	= (double) COUNT * LEN * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	double pps 		= (double) COUNT / dt_ms ;
	printf("Tranfer completed at %f Gbits/s in %f ms. ( %.2f k pps)\n", gbps,  dt_ms, pps);

return 0;
}
