#define _GNU_SOURCE
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include "udp.h"


char buffer[COUNT][LEN];

struct timespec startTime, endTime;    

struct sockaddr_in adr,a1;
int s;
int start = 0, run=1,clck=1;
int len;

struct 		mmsghdr msgs[VLEN_REC];
struct 		iovec iovecs[VLEN_REC];

int res[NTHREAD];
void* trecv(void*arg)
{
	int tid = *(int*)arg;
	int count = 0;
	printf("trecv %d\n",tid);
	
	while(1) 
	{
		recvfrom(s,buffer[tid],LEN,0, NULL, NULL);
		res[tid]++;

		int s=0;
		for (int i=0;i<NTHREAD; i ++) 
		{ 
			s += res[i];
		}
		printf("sum [%d] = %d\n",tid,s); 
	}
	
	
}


void main()
{

    int n,len,i=0;
    
	int start = 1;
    uint64_t opsn, psn = 0 ;
    unsigned int dropped = 0;

	select_cpu(RECV);

    s = socket(AF_INET,SOCK_DGRAM,0);
    if (s==-1) printf("erreur socket\n");
    adr.sin_family = AF_INET;
    adr.sin_port = htons(PORT);
    adr.sin_addr.s_addr = INADDR_ANY;

    int disable = 1;
	if (setsockopt(s, SOL_SOCKET, SO_NO_CHECK, (void*)&disable, sizeof(disable)) < 0) {
	    perror("setsockopt failed");
	}
	int optval = 1;
	setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	optval = 1024*1024*1024;
	setsockopt(s, SOL_SOCKET, SO_RCVBUF, &optval, sizeof(int));
	
    n = bind(s,(struct sockaddr *)(&adr),sizeof(struct sockaddr));
    if (n==-1) printf("bind error\n");


	for (int i=0;i<NTHREAD; i ++) 
		{ 
			res[i]=0;
		}
	clock_gettime(CLOCK_MONOTONIC, &startTime);
	
	
	int data[NTHREAD]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	pthread_t tid[NTHREAD];
	for (int j=0;j<NTHREAD; j ++) {
		printf("start recvfrom thread waiting data %d\n",j);
		pthread_create(&tid[j],NULL,trecv,&data[j]);
		
	}
	for (int j=0;j<NTHREAD; j ++)
		pthread_join(tid[j],NULL);

	printf("recvmmsg %s %s\n",buffer[0],buffer[1]);
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	
	double dt_ms 	= (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps 	= (double) COUNT * LEN * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	double pps 		= (double) COUNT / dt_ms ;

	printf("Tranfer completed at %f Gbits/s (%.2f pps)in %f ms. (%f %% dropped)\n",
			gbps, pps, dt_ms, ((double) dropped) /  COUNT * 100.0);
}
