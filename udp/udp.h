
#define VLEN_REC 		6
#define VLEN_SEND		1


#define NTHREAD 	8
//#define LEN 		(4096)
//#define COUNT 		(2*1024*128)


#define PORT 		2222

#define SRC			1
#define RECV		2


void select_cpu(int dir)
{
	cpu_set_t set;
	CPU_ZERO(&set);
	if (dir == SRC)
	{
		CPU_SET(1, &set);
		CPU_SET(3, &set);
		CPU_SET(5, &set);
		CPU_SET(7, &set);
		CPU_SET(9, &set);
		CPU_SET(11, &set);
	}
	else if (dir == RECV)
	{
		CPU_SET(1, &set);
		CPU_SET(3, &set);
		CPU_SET(5, &set);
		CPU_SET(7, &set);
		CPU_SET(9, &set);
		CPU_SET(11, &set);
		CPU_SET(13, &set);
		CPU_SET(15, &set);
	}
	sched_setaffinity(0, sizeof(set), &set);
	unsigned cpu = sched_getcpu();
	printf("thread using cpu %d  \n",cpu);
}
