/*
 * serveur UDP
 *
 * Wed Oct 13 10:33:46 CEST 2010 rp
 * 	cosmetic revision
 * RP 11/10/98
 * si plusieurs clients, servis à tour de role indistinctement par le meme
 * process

gcc udpserver.c -o udpserver

 */
#define _GNU_SOURCE
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <time.h>
#include <sched.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h>
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include "udp.h"
#include <cuda.h>
#include <cuda_runtime_api.h>
#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)

struct timespec startTime, endTime;

int n = 0;
int bufLen;
char *buffer;

void* print(void*arg)
{
    printf("revfrom num %d / %d\n",n,bufLen);

  while(1)
  {
    sleep(1);
    printf("revfrom num %d / %d\n",n,bufLen);
  }
}

void main(int argc, char**argv)
{
bufLen 		    = atoi(argv[1]);
int nIter 		= atoi(argv[2]);
int nLoop 		= atoi(argv[3]);
int page_size	= sysconf(_SC_PAGESIZE);
buffer 	      = (char*)memalign(page_size, bufLen*nIter );
// cudaMallocHost((void**) &buffer,  bufLen*nIter);
// cudaCheckErrors("cudaMallocHost fail");

  int s,res;
  struct sockaddr_in adr;

	select_cpu(RECV);

  s=socket(AF_INET,SOCK_DGRAM,0);
  if (s==-1) printf("erreur socket\n");

  adr.sin_family      = AF_INET;
  adr.sin_port        = htons(PORT);
  adr.sin_addr.s_addr = INADDR_ANY;

  res=bind(s,(struct sockaddr *)(&adr),sizeof(struct sockaddr));
  #define SOCKET_BUFFER_SIZE (1*1024*1024*1024UL) //2GB 2000
  size_t   val = SOCKET_BUFFER_SIZE;// bufLen*nIter;
  res=setsockopt(s, SOL_SOCKET, SO_RCVBUF, &val, sizeof(int));
  if (res==-1) printf("erreur setsockopt\n");


  pthread_t tid;
  int data;
  printf("recvfrom test v0.4 port %d\n",PORT);
  volatile int* addr1  = (int*)(buffer);
  volatile int* addr   = (int*)(buffer+(nIter-1)*bufLen);
  volatile char* a     = (buffer+(nIter-1)*bufLen);
  //pthread_create(&tid,NULL,print,&data);


  //VERY IMPORTANT NOTICE : LONG WARMUP REQUIRED FOR LIBVMA
  for(int i=0;i<65536;i++)
  {
    //printf("7\n");
    res = recvfrom(s, &buffer[0], bufLen, 0, NULL, NULL);
  }
  // printf("%x%x%x%x %d %d\n",  
  //        buffer[0],buffer[0+1],
  //        buffer[0+2],buffer[0+3],
  //        buffer[0+18],*(((int*)buffer)+0*bufLen/4+32));
  //printf("firts id %d\n",*(((int*)buffer +64)));


  // printf("PACKET %d hdr %x bunchId %d\n",
  // 						*(buffer+18) & 0xff,*(addr1+0),
  //             (*(buffer+7)) & 0xff+256*(*(buffer+6)) & 0xff);
  // printf("warmup done\n");
  //getchar();
  int firstId = *(addr1+64);



  n=0;

  for (int k=0; k<nLoop; k++) 
  {
    //clear TAG in last line of buffer
    buffer[(nIter-1)*bufLen]=0;
    // *(a+18)=0;
    // *(a+7)=0;
    // *(a+6)=0;

    for (int j=0; j<nIter; j++) 
    {
      res = recvfrom(s, &buffer[j*bufLen], bufLen, 0, NULL, NULL);
      

      if (n==0) 
      {
        clock_gettime(CLOCK_MONOTONIC, &startTime);
      } 
      //printf("received %d bytes %d times\n",res,j);

      if (res ==-1) printf("recvfrom error\n");

      n++;
    }
  }
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	double dt_ms 	= (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps 	= (double) nLoop * nIter * bufLen * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	double pps 		= (double) nLoop * nIter / dt_ms ;
	printf("Tranfer completed %d pkt at %f Gbits/s in %f ms. ( %.2f k pps) check TAG %s\n", 
      n, gbps, dt_ms, pps, &buffer[(nIter-1)*bufLen]);
	// printf("%d %d %d %f %f %.2f %x%x%x%x %d %u %u %u %u\n", bufLen, nIter, nLoop, gbps, dt_ms, pps, 
  //        buffer[(nIter-1)*bufLen],buffer[(nIter-1)*bufLen+1],
  //        buffer[(nIter-1)*bufLen+2],buffer[(nIter-1)*bufLen+3],
  //        buffer[(nIter-1)*bufLen+18],
  //        buffer[(nIter-1)*bufLen+32]&0xff,
  //        buffer[(nIter-1)*bufLen+32+1]&0xff,
  //        buffer[(nIter-1)*bufLen+32+2]&0xff,
  //        buffer[(nIter-1)*bufLen+32+3]&0xff
  //        );


  // printf("PACKET %d hdr %x bunchId %d\n",
  // 						*(a+18) & 0xff,*(addr+0),
  //             (*(a+7)) & 0xff+256*(*(a+6)) & 0xff);

  printf("%d deltaId/nIter %f\n",*(addr+64)-firstId,(float)((*(addr+64)-firstId))/nIter);

  //free(buffer);
}
