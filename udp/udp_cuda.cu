/*
 * serveur UDP
 *
 * Wed Oct 13 10:33:46 CEST 2010 rp
 * 	cosmetic revision
 * RP 11/10/98
 * si plusieurs clients, servis à tour de role indistinctement par le meme
 * process

gcc udpserver.c -o udpserver

 */
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>

#define PORT 2222
#define LEN 80

void main()
{
    int s,n,len,i=0;
    struct sockaddr_in adr;
    char buffer[LEN];
    struct hostent *h;


    s=socket(AF_INET,SOCK_DGRAM,0);
    if (s==-1) printf("erreur socket\n");
    adr.sin_family=AF_INET;
    adr.sin_port=htons(PORT);
    adr.sin_addr.s_addr=INADDR_ANY;

    n=bind(s,(struct sockaddr *)(&adr),sizeof(struct sockaddr));
    if (n==-1) printf("bind error\n");
    
    printf("udp data from %d.%d.%d.%d\n",
	    adr.sin_addr.s_addr & 0xFF,
	    (adr.sin_addr.s_addr >> 8) & 0xFF,
	    (adr.sin_addr.s_addr >> 16) & 0xFF,
	    adr.sin_addr.s_addr >> 24
	    );
    while(1){
		//réception
	    printf("udp server v1.1 waiting for request on %d port...\n",PORT);
	    n=recvfrom(s,buffer,LEN,0,(struct sockaddr *)(&adr),&len);
	    if (n==-1) printf("recvfrom error\n");
	    else buffer[n]=0;
	    }
}
