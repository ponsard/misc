/*


 */

#define _GNU_SOURCE
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <time.h>
#include <sched.h>
#include <malloc.h>
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>

#include "udp.h"


struct timespec startTime, endTime;




static unsigned long long rdtsc(void)
{
    unsigned int low, high;
    asm volatile("rdtsc" : "=a" (low), "=d" (high));
    return low | ((unsigned long long)high) << 32;
}


void main(int argc, char**argv)
{
int bufLen 		= atoi(argv[1]);
int nIter 		= atoi(argv[2]);
int nLoop 		= atoi(argv[3]);
int nanodelay	= atoi(argv[4]);

int page_size	= sysconf(_SC_PAGESIZE);
char *buffer 	=  memalign(page_size, bufLen*nIter );


    int s,s2,n,len,i=0;
    struct sockaddr_in adr,adr2;
	int nn;
	volatile int *p = &nn;
	
	//TAG
	for (int j=0; j<nIter; j ++)
		sprintf(&buffer[j*bufLen],"azertuiop %04d",j);

	select_cpu(SRC);

    s=socket(AF_INET,SOCK_DGRAM,0);
    if (s==-1) printf("erreur socket\n");
    adr.sin_family 		= AF_INET;
    adr.sin_port 		= htons(PORT);
    adr.sin_addr.s_addr	= inet_addr("192.168.3.14");

   	

    n=bind(s,(struct sockaddr *)(&adr),sizeof(struct sockaddr));
    // if (n==-1) printf("bind error\n");
    
    printf("sending udp data to %d.%d.%d.%d port %d\n",
	    adr.sin_addr.s_addr & 0xFF,
	    (adr.sin_addr.s_addr >> 8) & 0xFF,
	    (adr.sin_addr.s_addr >> 16) & 0xFF,
	    adr.sin_addr.s_addr >> 24, PORT
	    );

//warmup
//  n = sendto(s,&buffer[0],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[1],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[2],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[3],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[4],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[5],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
//  n = sendto(s,&buffer[6],1024,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
  for(int i=0;i<512;i++)
  	n = sendto(s,&buffer[0],bufLen,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));

printf("warmup done nIter %d,nLoop %d\n",nIter,nLoop);
   //getchar();

	clock_gettime(CLOCK_MONOTONIC, &startTime);
	for (int k=0; k<nLoop; k ++) 

	for (int j=0; j<nIter; j ++) 
	{
		//printf("nIter %d,nLoop %d %d\n",k,j,j+k*nIter);

		// buffer[j*bufLen+32+0] = (j+k*nIter) & 0xff;
		// buffer[j*bufLen+32+1] = (j+k*nIter)>>8 & 0xff;
		// buffer[j*bufLen+32+2] = (j+k*nIter)>>16 & 0xff;
		// buffer[j*bufLen+32+3] = (j+k*nIter)>>24 & 0xff;
		int* p = (int*) (buffer+j*bufLen);
		*(p+64) = j+k*nIter;
 		int n = sendto(s,buffer+j*bufLen,bufLen,0,(struct sockaddr *)(&adr),sizeof(struct sockaddr_in));
		//printf("sendto %d %d %d %d\n",buffer[j*bufLen+32+0],buffer[j*bufLen+32+1],buffer[j*bufLen+32+2],buffer[j*bufLen+32+3]);
	    if (n!=bufLen) printf("sendto error\n");
			//for (int i=0;i<750;i++) *p;
		unsigned long long start = rdtsc(),now;
	
		do 
		{
			now = rdtsc();
		} 
		while ((now - start) < nanodelay);
		//usleep(1);
		if(j==(nIter-1))
		{
				clock_gettime(CLOCK_MONOTONIC, &endTime);
				double dt_ms 	= (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
				double gbps 	= (double) nIter * bufLen * 8 / dt_ms * 1e3 / 1e9;
				double pps 		= (double) nIter / dt_ms ;
				printf("Tranfer completed at %f Gbits/s  %f pps. ( %.2f k pps)\r", gbps, pps);
				fflush(stdout);
				clock_gettime(CLOCK_MONOTONIC, &startTime);
		}
	}

free(buffer);
}
