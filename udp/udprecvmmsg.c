#define _GNU_SOURCE
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include "udp.h"


char buffer[COUNT][LEN];

struct timespec startTime, endTime;    

struct sockaddr_in adr,a1;
int s;
int start = 0, run=1,clck=1;
int len;

struct 		mmsghdr msgs[VLEN_REC];
struct 		iovec iovecs[VLEN_REC];

void main()
{

    int n,len,i=0;
    
	int start = 1;
    uint64_t opsn, psn = 0 ;
    unsigned int dropped = 0;

	select_cpu(RECV);

    s = socket(AF_INET,SOCK_DGRAM,0);
    if (s==-1) printf("erreur socket\n");
    adr.sin_family = AF_INET;
    adr.sin_port = htons(PORT);
    adr.sin_addr.s_addr = INADDR_ANY;

    int disable = 1;
	if (setsockopt(s, SOL_SOCKET, SO_NO_CHECK, (void*)&disable, sizeof(disable)) < 0) {
	    perror("setsockopt failed");
	}
	int optval = 1;
//	setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
//	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	optval = 1024*1024*1024;
	setsockopt(s, SOL_SOCKET, SO_RCVBUF, &optval, sizeof(int));
	
    n = bind(s,(struct sockaddr *)(&adr),sizeof(struct sockaddr));
    if (n==-1) printf("bind error\n");

    memset(msgs, 0, sizeof(msgs));
	for (int i = 0; i < VLEN_REC; i++) {
	   iovecs[i].iov_len = LEN;
	   iovecs[i].iov_base = buffer[i];
	   msgs[i].msg_hdr.msg_iov    = &iovecs[i];
	   msgs[i].msg_hdr.msg_iovlen = 1;
	}
	clock_gettime(CLOCK_MONOTONIC, &startTime);
	
	printf("recvmmsg waiting data\n");
	for (int j=0;j<COUNT; j += VLEN_REC) {
		printf("j=%d\n",j);
		for (int i = 0; i < VLEN_REC; i++) {
		   iovecs[i].iov_base = buffer[j+i];
		}

		int retval = recvmmsg(s, msgs, VLEN_REC, 0, NULL);
		if (retval != VLEN_REC) {
			perror("recvmmsg()");
			exit(EXIT_FAILURE);
		}
	}
	

	printf("recvmmsg %s %s\n",buffer[0],buffer[1]);
	clock_gettime(CLOCK_MONOTONIC, &endTime);
	
	double dt_ms 	= (endTime.tv_nsec-startTime.tv_nsec)/1000000.0 + (endTime.tv_sec-startTime.tv_sec)*1000.0;
	double gbps 	= (double) COUNT * LEN * 8 / dt_ms * 1e3 / 1024.0 / 1024.0 / 1024.0;
	double pps 		= (double) COUNT / dt_ms ;

	printf("Tranfer completed at %f Gbits/s (%.2f pps)in %f ms. (%f %% dropped)\n",
			gbps, pps, dt_ms, ((double) dropped) /  COUNT * 100.0);
}
